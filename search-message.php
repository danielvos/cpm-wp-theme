<?php
/**
 * The template for displaying search results of message.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package crosspoint
 */

get_header(); ?>

    <div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <h1 class="page-title">Messages</h1>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 text-right sort-buttons">
            <div class="btn-group btn-group-sm">
              <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Browse By Series   <span class="fa fa-caret-down"></span></a>
              <?php
                $series_category = get_terms( 'ctc_sermon_series' );
                if ( ! empty( $series_category ) && ! is_wp_error( $series_category ) ){
                  echo '<ul class="dropdown-menu" role="menu">';
                  foreach ( $series_category as $secat ) {                      
                    echo '<li><a href="' . get_term_link( $secat->slug, 'ctc_sermon_series' ) . '">' . $secat->name . '</a></li>';
                  }
                 echo '</ul>';
                }
            ?>
            </div>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 text-right sort-buttons">
            <div class="btn-group btn-group-sm">
              <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Browse By Speaker   <span class="fa fa-caret-down"></span></a>
              <?php
                $speaker_category = get_terms( 'ctc_sermon_speaker' );
                if ( ! empty( $speaker_category ) && ! is_wp_error( $speaker_category ) ){
                  echo '<ul class="dropdown-menu" role="menu">';
                  foreach ( $speaker_category as $scat ) {                      
                    echo '<li><a href="' . get_term_link( $scat->slug, 'ctc_sermon_speaker' ) . '">' . $scat->name . '</a></li>';
                  }
                 echo '</ul>';
                }
            ?>
            </div>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 sort-buttons">
            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                <input type="hidden" name="search_type" value="messages" />
                <div class="input-group input-group-sm">
                  <input type="text" name="s" id="s" class="form-control" placeholder="Search Messages" />
                </div>
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ctc_sermon' ); ?>">Messages</a>
              </li>                     
              <li class="active"><?php printf( esc_html__( 'Search Results for: %s', 'crosspoint' ), '<span>' . get_search_query() . '</span>' ); ?></li>
            </ul>
          </div>
        </div>    
        <div class="row">    
          <!-- MAIN CONTENT AREA -->
        <div class="col-xs-12">
          <h2 class="section-title"><?php printf( esc_html__( 'Search Results for: %s', 'crosspoint' ), '<span>' . get_search_query() . '</span>' ); ?></h2>    
          <?php if ( have_posts() ) : ?>
            <ul class="list-group message-list">
              <?php  while ( have_posts() ) : the_post(); $speaker = get_the_terms( $post->ID, 'ctc_sermon_speaker' );  ?>
                <li class="list-group-item">
                  <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  <span><i class="fa fa-calendar fa-fw"></i> <?php echo get_the_date('F j, Y'); ?></span>
                  <span><i class="fa fa-fw fa-microphone"></i><?php echo $speaker[0]->name; ?></span>
                </li>
              <?php endwhile; ?>   
            </ul>
          <?php else: ?>
            <h3>Nothing Found!</h3>
          <?php endif; ?>
        </div>            
        </div>
      </div>
    </div> 

<?php
get_footer();
