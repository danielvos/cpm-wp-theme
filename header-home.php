<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package crosspoint
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" type="image/png" href="<?php echo cs_get_option( 'favicon' ); ?>" />

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=213707358748041";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

  <div class="cover" id="home-header-section">
      <!-- <div class="cover-image" style="background-image : url('<?php echo get_template_directory_uri(); ?>/images/Christmas2017_HomePageWelcomePhoto_mini.jpg')"></div> -->
      <div class="cover-image" style="background-image : url('<?php echo get_template_directory_uri(); ?>/images/HomePageWelcomePhoto.jpg')"></div>
      <div class="navbar navbar-default navbar-static-top nav-bar-main">
          <div class="container">
            <div class="navbar-header">
              <a href="<?php echo home_url( '/' ); ?>" class="navbar-brand logo-container"><img height="60" alt="<?php bloginfo( 'name' ); ?>" src="<?php echo cs_get_option( 'logo' ); ?>"></a>
            </div>
            <?php
                  wp_nav_menu( array(
                      'menu'  => 'primary',
                      'theme_location'    => 'primary',
                      'depth'             => 2,
                      'container'         => 'div',
                      'container_id'      => 'navbar-ex-collapse',
                      'container_class'   => 'collapse navbar-collapse',
                      'menu_class'        => 'navbar navbar-default navbar-static-top nav-bar-main',
                      'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                      'walker'            => new wp_bootstrap_navwalker())
                  );
              ?>
          </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-xs-10 col-xs-offset-1 text-center home-header-title">
            <h1 class="home-header-title">Welcome to CrossPoint</h1>
            <p class="home-header-subtitle">1301 12th Street, Modesto<br/>
                Sundays at 9 &amp; 10:45 am</p>
            <p> 
            <a href="<?php echo site_url(); ?>/first-visit/" class="btn btn-lg btn-primary">First Visit</a>
            <a href="<?php echo site_url(); ?>/next-steps/" class="btn btn-lg btn-primary">Next Steps</a>
            </p>

            <!-- <h1 class="home-header-title">Christmas at CrossPoint</h1>
            <p class="home-header-subtitle">Sundays in December<br/>
                9 & 10:45 am</p>
            <p> 
            <a href="<?php echo site_url(); ?>/ministry/welcome-home/" class="btn btn-lg btn-primary">Find Out More</a>
            </p> -->
         </div>           
        </div>
      </div>
    </div>

	<div id="content" class="site-content">