<?php
/**
 * Template Name: Page Builder
 *
 * This is the template that displays all message series.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

    <?php while(have_posts()) : the_post(); ?>
      <!-- Content from Page Bulider -->
      <?php the_content(); ?>
    <?php endwhile; ?>

<?php
get_footer();