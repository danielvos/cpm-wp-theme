<?php
/**
 * Template Name: Message Series
 *
 * This is the template that displays all message series.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */
$page = isset($_GET['series']) ? $_GET['series'] : 1;
$offset = (($page == 1) ? 0 : ($page - 1) * 9);

get_header(); ?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="page-title">Browse Messages by Series</h1>
          </div>
        </div>
      </div>
    </div>
      
    <div class="section message-series-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ctc_sermon' ); ?>">Messages</a>
              </li>                
              <li class="active">Browse Messages by Series</li>
            </ul>
          </div>
        </div>          

        <div class="row">
        	<?php 
            $results = get_series_categories();

            $catsArray = [];
            foreach($results as $key => $res){
              $catsArray[] = $key;
            }

            $series = get_terms( array(
              'taxonomy' => 'ctc_sermon_series',
              'number'  =>  9,
              'offset'  =>  $offset,
              'include' => $catsArray,
              'orderby'  => 'include',
            ) );

                if ( ! empty( $series ) && ! is_wp_error( $series ) ):
                  $count = 1;
                  foreach ( $series as $ser) :
            ?>
				      <div class="col-sm-4 series-box"<?php echo (($count == 4 || $count == 7) ? ' style="clear: left;"' : ''); ?>>
		            <a href="<?php echo get_term_link( $ser->slug, 'ctc_sermon_series' ) ?>">
		            <div class="series-box-container">	
                 <?php if(!empty(types_render_termmeta( "message-series-image", array("term_id" => $ser->term_id)))): 
                    echo types_render_termmeta( "message-series-image", array("term_id" => $ser->term_id, "alt" => $ser->name, "class" => "img-responsive", "width" => "768", "height" => "432") );
                  ?>
                  <?php else: ?>        
                    <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive" alt="<?php echo $ser->name; ?>"> 
                  <?php endif; ?>
		              <div class="box-content">
		                <h5><?php echo $ser->name; ?></h5>
		              </div>
		            </div>
		            </a>
		        </div>
        	<?php
                  $count++;
                  endforeach;
                endif;
            ?>
        </div>      


		<div class="row">
        	<div class="col-md-12">
				<?php message_series_navigation($results); ?>
			</div>
		</div>

      </div>
    </div>

<?php
get_footer();