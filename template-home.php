<?php
/**
 * Template Name: Home
 *
 * This is the template that displays all message series.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header('home'); ?>

  <div class="section" style="background-color: white;">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-center">
            <h3>We’re here to help each other<br/>
            worship, live, and rescue like Jesus.</h3>
          </div>
        </div>
      </div>
    </div>
      
    <!-- Get Connected Section -->  
    <div class="section section-info">
      <div class="background-image background-image-fixed" style="background-image : url('<?php echo get_template_directory_uri(); ?>/images/HomePageGetConnected.jpg')"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="section-title section-title-white">Get Connected</h2>
            <p>You don't have to go through life alone, and we’re here to make sure you don't.<br/>
            Get started by finding a community at CrossPoint.</p>
            <a href="<?php echo site_url(); ?>/ministry-category/life-stage/" class="btn btn-primary">Find a Community</a>
          </div>
        </div>
      </div>
    </div>
    
    <?php        
      $sermon_args = array(
        'post_type' => 'ctc_sermon',
        'post_status' => 'publish',
        'posts_per_page' => 1,
        'orderby'   => 'id',
        'order' => 'DESC',
      );
      $message = new WP_Query( $sermon_args );
    if ($message->have_posts()) :    
      while ( $message->have_posts() ) : $message->the_post();  
      $series = get_the_terms( $post->ID, 'ctc_sermon_series' );  
    ?>
    <!-- Messages Section -->  
    <div class="section home-sermon-section">
      <div class="background-image" style="background-image: url('<?php echo types_render_termmeta( "message-series-image", array("term_id" => $series[0]->term_id, "alt" => $series[0]->name, "class" => "sermon-series-cover img-responsive center", "output" => "raw") ); ?>');"></div>
      <div class="section-overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-8 col-xs-offset-2 text-center">
            <h2 class="section-title section-title-white">Current Series</h2>
            <div class="row">
                <p><a href="<?php echo site_url(); ?>/ctc_sermon/">
                <?php if(!empty(types_render_termmeta( "message-series-image", array("term_id" => $series[0]->term_id)))): 
                  echo types_render_termmeta( "message-series-image", array("term_id" => $series[0]->term_id, "alt" => $series[0]->name, "class" => "sermon-series-cover img-responsive center") );
                ?>
                <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="sermon-series-cover img-responsive center" alt="<?php echo $series[0]->name; ?>"> 
                <?php endif; ?></a></p>
            </div>
            <div class="row">
              <div class="col-md-6 text-center">
                <a href="<?php echo site_url(); ?>/ctc_sermon/" class="btn btn-primary">Watch Latest Message</a>
              </div>
              <div class="col-md-6 text-center">
                <a href="<?php echo site_url(); ?>/sermon-series/" class="btn btn-primary">Search Archives</a>
              </div>
            </div>   
          </div>   
        </div>
      </div>
    </div>
    <?php endwhile; endif; wp_reset_query(); ?>

    <?php 
        $ev1 = [];
        $ev2 = [];

        $events1_args = array(
          'post_type'     => 'ctc_event',                
          'meta_query' => array(
            array(
              'key' => '_ctc_event_end_date',
              'value' => date_i18n( 'Y-m-d' ),
              'compare' => '>=',
              'type' => 'DATE'
            ),
          ),
          'meta_key'      => '_ctc_event_start_date_start_time',
          'meta_type'     => 'DATETIME',
          'orderby'     => 'meta_value',
          'order'       => 'ASC',
          'posts_per_page' => 3,
          'fields' => 'ids',
          'tax_query' => array(
              array(
                  'taxonomy' => 'ctc_event_category',
                  'field'    => 'slug',
                  'terms'    => 'featured',
              ),
          ),
        );

        $events1 = new WP_Query( $events1_args );
        $e1count = $events1->post_count;
        $ev1 = $events1->posts;

        if($e1count < 3){
            $events2_args = array(
            'post_type'     => 'ctc_event',                
            'meta_query' => array(
              array(
                'key' => '_ctc_event_end_date',
                'value' => date_i18n( 'Y-m-d' ),
                'compare' => '>=',
                'type' => 'DATE'
              ),
            ),
            'meta_key'      => '_ctc_event_start_date_start_time',
            'meta_type'     => 'DATETIME',
            'orderby'     => 'meta_value',
            'order'       => 'ASC',
            'posts_per_page' => (3 - $e1count),
            'fields' => 'ids',
            'tax_query' => array(
                array(
                    'taxonomy' => 'ctc_event_category',
                    'field'    => 'slug',
                    'terms'    => 'featured',
                    'operator' => 'NOT IN',
                ),
            ),
          );

          $events2 = new WP_Query( $events2_args );
          $ev2 = $events2->posts;
        }        

        $event_ids = array_merge( $ev1, $ev2 );
        $events = new WP_Query(array(
            'post_type' => 'ctc_event',
            'post__in'  => $event_ids, 
            'meta_query' => array(
                array(
                  'key' => '_ctc_event_end_date',
                  'value' => date_i18n( 'Y-m-d' ),
                  'compare' => '>=',
                  'type' => 'DATE'
                ),
              ),
              'meta_key'      => '_ctc_event_start_date_start_time',
              'meta_type'     => 'DATETIME',
              'orderby'     => 'meta_value',
              'order'       => 'ASC',
        ));

        if ($events->have_posts()) :       
    ?>    
    <!-- Events Section -->  
    <div class="section event-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
              <h2 class="section-title">Coming Up</h2>
          </div>
        </div>
        <div class="row" style="color: black;">
          <div class="col-xs-12 event-container">
            <?php while ( $events->have_posts() ) : $events->the_post(); ?>
            <div class="row event-row">
              <div class="col-xs-12 col-sm-8">
                <a href="<?php the_permalink(); ?>"><h3 class="event-title"><?php the_title(); ?></h3></a>
                <ol class="list-inline event-details-list">
                  <?php if(get_post_meta( $post->ID, '_ctc_event_start_date', true) != get_post_meta( $post->ID, '_ctc_event_end_date', true)): ?>
                  <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("M d", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?> - <?php echo date("M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_end_date', true))); ?></li>
                  <?php else: ?>
                  <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("D, M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?></li>
                  <?php endif; ?>
                  <?php if(get_post_meta( $post->ID, '_ctc_event_hide_time_range', true) == 1): ?>
                  <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo get_post_meta( $post->ID, '_ctc_event_time', true); ?></li>
                  <?php else: ?>
                  <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_start_time', true))); ?> - <?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_end_time', true))); ?></li>
                  <?php endif; ?>
                  <?php if(!empty(get_post_meta( $post->ID, '_ctc_event_venue', true))): ?>
                  <li><i class="event-detail-icon fa fa-fw fa-lg fa-location-arrow"></i><?php echo get_post_meta( $post->ID, '_ctc_event_venue', true); ?></li>
                  <?php endif; ?>
                  <li><i class="fa fa-2x fa-fw fa-refresh event-detail-icon"></i><?php echo ucfirst(get_post_meta( $post->ID, '_ctc_event_recurrence', true)); ?></li>
                </ol>
                <?php the_excerpt(); ?>
              </div>
              <div class="hidden-xs col-sm-12 col-sm-4 event-details-image-container">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>      
                  <img src="<?php echo $image[0]; ?>" class="event-details-image img-responsive" alt="<?php the_title(); ?>">        
                <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="event-details-image img-responsive" alt="<?php the_title(); ?>"> 
                <?php endif; else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="event-details-image img-responsive" alt="<?php the_title(); ?>"> 
                <?php endif; ?>  
              </div>
            </div>
            <?php endwhile; ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <a href="<?php echo site_url(); ?>/ctc_event/" class="btn btn-primary">View All Events</a>
          </div>
        </div>          
      </div>
    </div>
    <?php endif; wp_reset_query(); ?>

    <!-- Rescue Section -->  
    <div class="section white-text-section" id="rescue-section">
      <div class="background-image background-image-fixed" style="background-image : url('<?php echo get_template_directory_uri(); ?>/images/rescue-section-photo_mini-tiny.jpg')"></div>
      <div class="section-overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="section-title section-title-white">A Culture of Rescue</h2>
            <p class="text-center">We have been rescued by Jesus,<br/>
            so that we can join Him in rescuing others.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 text-center">
            <h4>Local Rescue</h4>
            <p>We bring God’s love and hope to all who are lost and lonely in the Modesto area. We meet needs by partnering with other ministries, and equipping and sending people out to be rescuers in their own community.</p>
            <a href="<?php echo site_url(); ?>/ministry/local-rescue/" class="btn btn-primary">Get More Info</a>
          </div>
          <div class="col-md-6 text-center">
            <h4>Global Rescue</h4>
            <p>There are millions of people across the globe in need of the hope and freedom that comes from a saving relationship with Jesus. By partnering with global ministries and sending out our own Global Staff, we are taking the Gospel to the nations.</p>
            <a href="<?php echo site_url(); ?>/ministry/global-rescue/" class="btn btn-primary">Get More Info</a>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Serve Section -->  
    <div class="section white-text-section" id="serve-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="section-title section-title-white">Serve</h2>
            <p>One of the hallmarks of belonging to a church family is serving together,<br/>
            investing in the lives of others. Find a place to get involved.</p>
            <a href="<?php echo site_url(); ?>/ministry-category/serve/" class="btn btn-primary">View Opportunities</a>
          </div>
        </div>
      </div>
    </div>

<?php
get_footer();
