<?php
/**
 * crosspoint functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package crosspoint
 */

// Include Church Theme Framework
require_once get_template_directory() .'/inc/church-theme/framework.php';

// Include Codestar Framework
require_once get_template_directory() .'/inc/codestar-framework/cs-framework.php';

// Add support of Codestar Framework
define( 'CS_ACTIVE_FRAMEWORK',  true  );
define( 'CS_ACTIVE_METABOX',    false );
define( 'CS_ACTIVE_TAXONOMY',   false );
define( 'CS_ACTIVE_SHORTCODE',  false );
define( 'CS_ACTIVE_CUSTOMIZE',  false );

// Include Bootstrap Navwalker
require_once get_template_directory() .'/inc/wp_bootstrap_navwalker.php';

// Include Twitter Auth
require_once get_template_directory() .'/inc/twitteroauth/twitteroauth.php';

// Add support of Twitter Auth
$twitter_ck = 'tSGcO1zw9LtKy51OjDzuimfR0';
$twitter_cs = 'FlcVDabrnoRXBdAP1TLZGl7yE4ikeWKTCbE3v7b5OFjrBQVKxE';
$twitter_at = '179928431-ezHpuXnsPDsZ6eTkwEQuS3dq79yFkGbsv0ZRbhOZ';
$twitter_ats = 'q18vKTZBE9I6Ahdp56wDo7AwzRH623Qg6aPuxvdET4NHr';
$twitterCon = new TwitterOAuth($twitter_ck, $twitter_cs, $twitter_at, $twitter_ats);

if ( ! function_exists( 'crosspoint_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function crosspoint_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on crosspoint, use a find and replace
	 * to change 'crosspoint' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'crosspoint', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'crosspoint' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'crosspoint_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Crop Image for Featured Image
	add_image_size( 'featured-image', 768, 432, true );

	add_theme_support( 'church-theme-content' );

	// Church Theme Framework - Sermons
	add_theme_support( 'ctc-sermons', array(
	    'taxonomies' => array(
	        'ctc_sermon_topic',
	        'ctc_sermon_book',
	        'ctc_sermon_series',
	        'ctc_sermon_speaker',
	        'ctc_sermon_tag',
	    ),
	    'fields' => array(
	        '_ctc_sermon_has_full_text',
	        '_ctc_sermon_video',
	        '_ctc_sermon_audio',
	        '_ctc_sermon_pdf',
	    ),
	    'field_overrides' => array()
	) );

	// Church Theme Framework - Events
	add_theme_support( 'ctc-events', array(
	    'taxonomies' => array(
	        'ctc_event_category',
	    ),
	    'fields' => array(
	        '_ctc_event_start_date',
	        '_ctc_event_end_date',
	        '_ctc_event_start_time',
	        '_ctc_event_end_time',
	        '_ctc_event_hide_time_range',
	        '_ctc_event_time',                // time description
	        '_ctc_event_recurrence',
	        '_ctc_event_recurrence_end_date',
	        '_ctc_event_venue',
	        '_ctc_event_address',
	        '_ctc_event_show_directions_link',
	        '_ctc_event_map_lat',
	        '_ctc_event_map_lng',
	        '_ctc_event_map_type',
	        '_ctc_event_map_zoom',
	        '_ctc_event_registration_url',
	    ),
	    'field_overrides' => array()
	) );

	// Church Theme Framework - People
	add_theme_support( 'ctc-people', array(
	    'taxonomies' => array(
	        'ctc_person_group',
	    ),
	    'fields' => array(
	        '_ctc_person_position',
	        '_ctc_person_phone',
	        '_ctc_person_email',
	        '_ctc_person_urls',
	    ),
	    'field_overrides' => array()
	) );

	// Church Theme Framework - Locations
	add_theme_support( 'ctc-locations', array(
	    'taxonomies' => array(),
	    'fields' => array(
	        '_ctc_location_address',
	        '_ctc_location_show_directions_link',
	        '_ctc_location_map_lat',
	        '_ctc_location_map_lng',
	        '_ctc_location_map_type',
	        '_ctc_location_map_zoom',
	        '_ctc_location_phone',
	        '_ctc_location_email',
	        '_ctc_location_times',
	    ),
	    'field_overrides' => array()
	) );

	// Church Theme Framework - Widgets
	add_theme_support( 'tbcf', array(
    'widgets' => array(
      'events' => array(
        'fields' => array( 'title', 'number', 'thumbnail', 'date', 'time' )
      ),
      'locations' => array(
        'fields' => array( 'title', 'thumbnail', 'address', 'times', 'map' )
      ),
      'people' => array(
        'fields' => array( 'title', 'group', 'number', 'thumbnail', 'excerpt', 'position' )
      ),
      'sermons' => array(
        'fields' => array( 'title', 'number', 'thumbnail', 'excerpt', 'date' )
      )
    )
  ) );
}
endif;
add_action( 'after_setup_theme', 'crosspoint_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function crosspoint_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'crosspoint_content_width', 640 );
}
add_action( 'after_setup_theme', 'crosspoint_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function crosspoint_widgets_init() {
	// Right Sidebar 
	register_sidebar( array(
		'name'          => esc_html__( 'Right Sidebar', 'crosspoint' ),
		'id'            => 'right-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'crosspoint' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Ministry Sidebar 
	register_sidebar( array(
		'name'          => esc_html__( 'Ministry Sidebar', 'crosspoint' ),
		'id'            => 'ministry-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'crosspoint' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	// Footer Sidebar 1
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 1', 'crosspoint' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'crosspoint' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p style="display:none;"><strong>',
		'after_title'   => '</strong></p>',
	) );

	// Footer Sidebar 2
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 2', 'crosspoint' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'crosspoint' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p><strong>',
		'after_title'   => '</strong></p>',
	) );

	// Footer Sidebar 3
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 3', 'crosspoint' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'crosspoint' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p><strong>',
		'after_title'   => '</strong></p>',
	) );

	// Footer Sidebar 4
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Sidebar 4', 'crosspoint' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'crosspoint' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<p><strong>',
		'after_title'   => '</strong></p>',
	) );
}
add_action( 'widgets_init', 'crosspoint_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function crosspoint_scripts() {
	wp_enqueue_style( 'crosspoint-style', get_stylesheet_uri() );
	wp_enqueue_style( 'crosspoint-bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
	wp_enqueue_style( 'crosspoint-fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );	
	wp_enqueue_style( 'crosspoint-jplayer', '//cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/skin/blue.monday/css/jplayer.blue.monday.min.css' );
	wp_enqueue_style( 'crosspoint-main', get_template_directory_uri() . '/css/main.css' );
	wp_enqueue_style( 'crosspoint-custom', get_template_directory_uri() . '/css/custom.css' );

	wp_enqueue_script('jquery');
	wp_enqueue_script( 'crosspoint-bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array(), '', true );
	wp_enqueue_script( 'crosspoint-jquery.carouFredSel', get_template_directory_uri() . '/js/jquery.carouFredSel.js', array(), '', true );
	wp_enqueue_script( 'crosspoint-jplayer', '//cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/jplayer/jquery.jplayer.min.js', array(), '', true );
	wp_enqueue_script( 'crosspoint-main', get_template_directory_uri() . '/js/main.js', array(), '', true );

	wp_enqueue_script( 'crosspoint-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '', true );
	wp_enqueue_script( 'crosspoint-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'crosspoint_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


// Customize Read More Button
function new_excerpt_more($more) {
	global $post;
	return ' <a href="'. get_permalink($post->ID) . '" class="blog-read-more">Read More</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Customize Post Excerpt Length
function custom_excerpt_length($length){
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Numeric Post Navigation/Pagination
function numeric_posts_navigation($page = NULL) {
	// if( is_singular() )
	// 	return;

	global $wp_query;

	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	if ( $paged >= 1 )
		$links[] = $paged;

	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<ul class="pager">' . "\n";

	if ( get_previous_posts_link() )
		printf( '<li class="previous">%s</li>' . "\n", get_previous_posts_link( '&#x2190; Prev' ) );

	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>...</li>';
	}

	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>...</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	if ( get_next_posts_link() )
		printf( '<li class="next">%s</li>' . "\n", get_next_posts_link( 'Next &#x2192;' ) );

	echo '</ul>' . "\n";

}

// Add Theme Option on WP Toolbar Menu
function theme_option_link($wp_admin_bar) {
	$args = array(
		'id' => 'crosspoint-options',
		'title' => '<i class="dashicons-before dashicons-admin-settings" style="position: relative;top: 5px;"></i> Theme Options', 
		'href' => admin_url() . 'admin.php?page=crosspoint', 
	);
	$wp_admin_bar->add_node($args);
}
add_action('admin_bar_menu', 'theme_option_link', 999);

// Fix Sticky Posts Exceed Posts Per Page Limit Issue
function fix_posts_limit($query) {
    if ($query->is_main_query() && is_home()) {
        $posts_per_page = 9;
        $sticky_posts = get_option( 'sticky_posts' );

        if (is_array($sticky_posts) && !$query->is_paged()) {
            $sticky_count = count($sticky_posts);

            if ($sticky_count < $posts_per_page) {
                $query->set('posts_per_page', $posts_per_page - $sticky_count);
            }
            else {
                $query->set('posts_per_page', 1);
            }
        }
        else {
            $query->set('posts_per_page', $posts_per_page);
        }
    }
}
add_action('pre_get_posts', 'fix_posts_limit');

// Choose Template for Search Result of Events/Message/Person
function template_chooser($template) {    
	global $wp_query;   
	$post_type = get_query_var('post_type'); 

	if(!empty($_GET['search_type'])){  
		if( $_GET['search_type'] == 'events' || !empty($_GET['category']) ){
			return locate_template('search-event.php'); 
		}  

		if( $_GET['search_type'] == 'messages' ){
			return locate_template('search-message.php'); 
		}  

		if( $_GET['search_type'] == 'person' ){
			return locate_template('search-person.php'); 
		}

		if( $_GET['search_type'] == 'ministry' ){
			return locate_template('search-ministry.php'); 
		}    
	}

	return $template;   
}
add_filter('template_include', 'template_chooser');    

// Send Post Type for Event/Message/Person Search
function events_search_filter($query) {
	if(!empty($_GET['search_type'])){  
	    if( $_GET['search_type'] == 'events' ){
	    	if( $query->is_main_query() ){
	        	$query->set('post_type', array('ctc_event'));
	    	}
	    }

	    if( $_GET['search_type'] == 'messages' ){
	    	if( $query->is_main_query() ){
		        $query->set('post_type', array('ctc_sermon'));
		    }
	    }

	    if( $_GET['search_type'] == 'person' ){
	    	if( $query->is_main_query() ){
		        $query->set('post_type', array('ctc_person'));
		    }
	    }

	    if( $_GET['search_type'] == 'ministry' ){
	    	if( $query->is_main_query() ){
		        $query->set('post_type', array('ministry'));
		    }
	    }
	}

	return $query;
}
add_filter('pre_get_posts', 'events_search_filter');

// Send Post Type, Category for Event Search by Category
function event_category_search_filter($query) {
    if( !empty($_GET['category']) ){
    	if( $query->is_main_query() ){
        	$query->set('post_type', array('ctc_event'));

        	 $tax_query = array(
				'taxonomy' => 'ctc_event_category', 
				'field' => 'slug', 
				'terms' => $_GET['category']);
			$query->tax_query->queries[] = $tax_query; 
	   		$query->query_vars['tax_query'] = $query->tax_query->queries;
    	}    	
    }

	return $query;
}
add_filter('pre_get_posts', 'event_category_search_filter');

// Message Series Navigation/Pagination
function message_series_navigation($results) {
	$adjacents = 3;
	$limit = 9;
	$total_pages = count($results);

	$targetpage = "";
	
	$page = isset($_GET['series']) ? $_GET['series'] : 1;
	if($page) 
		$start = ($page - 1) * $limit;
	else
		$start = 0;

	if ($page == 0) $page = 1;
	$prev = $page - 1;
	$next = $page + 1;
	$lastpage = ceil($total_pages / $limit);
	$lpm1 = $lastpage - 1;
	
	$pagination = "";
	if($lastpage > 1){	
		$pagination .= "<ul class=\"pager\">";
		if ($page > 1) 
			$pagination.= "<li class=\"previous\"><a href=\"$targetpage?series=$prev\">&#x2190; Prev</a></li>";
		else
			$pagination.= "<li class=\"previous\"><span class=\"disabled\">&#x2190; Prev</span></li>";	
		

		if ($lastpage < 7 + ($adjacents * 2))
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<li><span class=\"current\">$counter</span></li>";
				else
					$pagination.= "<li><a href=\"$targetpage?series=$counter\">$counter</a></li>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))
		{

			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><span class=\"current\">$counter</span></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?series=$counter\">$counter</a></li>";					
				}
				$pagination.= "...";
				$pagination.= "<li><a href=\"$targetpage?series=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?series=$lastpage\">$lastpage</a></li>";		
			}

			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage?series=1\">1</a></li>";
				$pagination.= "<a href=\"$targetpage?series=2\">2</a></li>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><span class=\"current\">$counter</span></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?series=$counter\">$counter</a></li>";					
				}
				$pagination.= "...";
				$pagination.= "<li><a href=\"$targetpage?series=$lpm1\">$lpm1</a>";
				$pagination.= "<li><a href=\"$targetpage?series=$lastpage\">$lastpage</a>";		
			}

			else
			{
				$pagination.= "<li><a href=\"$targetpage?series=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetpage?series=2\">2</a></li>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><span class=\"current\">$counter</span></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?series=$counter\">$counter</a></li>";					
				}
			}
		}
		
		if ($page < $counter - 1) 
			$pagination.= "<li class=\"next\"><a href=\"$targetpage?series=$next\">Next &#x2192;</a></li>";
		else
			$pagination.= "<li class=\"next\"><span class=\"disabled\">Next &#x2192;</span></li>";
		$pagination.= "</ul>\n";		
	}

	echo $pagination;
}

// Parse YouTube/Vimeo Video ID from URL for Embed
function generate_video($url) {
	if(strpos($url, 'youtube') !== false){
		parse_str( parse_url( $url, PHP_URL_QUERY ), $results );
		$iframe = '<iframe class="embed-responsive-item" id="playerVideo" src="https://www.youtube.com/embed/' . $results['v'] . '?enablejsapi=1&html5=1" allowfullscreen="true"></iframe>';		
	}
	elseif(strpos($url, 'vimeo') !== false){
		$iframe = '<iframe class="embed-responsive-item video-vimeo" id="playerVideo" src="https://player.vimeo.com/video/' . (int) substr(parse_url($url, PHP_URL_PATH), 1) . '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
	}
	else{
		$iframe = '';
	}

	return $iframe;    	
}

// URL check for Facebook & Twitter, return username if TwitterFacebook
function checkFacebookTwitter($url) {
	if( preg_match('#https?\://(?:www\.)?facebook\.com/(\d+|[A-Za-z0-9\.\-]+)/?#', $url, $facebook) ){
		return $url;
	}
	elseif( preg_match("|https?://(www\.)?twitter\.com/(#!/)?@?([^/]*)|", $url, $twitter) ){
		return $twitter[3];
	}
	else {
		return false;
	}
}

// Get Series Categories by Post Date Order
function get_series_categories() { 
	$terms = get_terms('ctc_sermon_series', array('fields'=>'ids') );
	$tax_query = array(
		'taxonomy' => 'ctc_sermon_series',
    	'field' => 'id',
    	'terms' => $terms,
  	);

	$args = array('post_type' => 'ctc_sermon', 'posts_per_page' => -1, 'tax_query' => array($tax_query));
	$q = new WP_Query( $args );
	$posts_ordered = array();
	$done = 0;
	
	while ( $q->have_posts() ) : $q->the_post();
		global $post;
		$series = get_the_terms($post->ID, 'ctc_sermon_series');
		$serie = array_shift( $series );
		
		if ( ! isset($posts_ordered[$serie->term_id]) )
			$posts_ordered[$serie->term_id] = array('term' => $serie);
		
		if ( ! isset($posts_ordered[$serie->term_id]['posts']) )
			$posts_ordered[$serie->term_id]['posts'] = array();
			$posts_ordered[$serie->term_id]['posts'][] = $post;
	endwhile;	
	wp_reset_postdata();
	
	return $posts_ordered;
}

function google_maps_script() {
    $map_key = tbcf_gmaps_api_key();
    wp_deregister_script( 'google-maps' );
    wp_enqueue_script( 'google-maps', '//maps.googleapis.com/maps/api/js?key='. $map_key .'&libraries=places&sensor=false-', array(), '', true );
}
add_action( 'wp_print_scripts', 'google_maps_script', 100 );
