<?php
/**
 * The template for displaying single event.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="page-title"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ctc_event' ); ?>">Calendar</a>
              </li>  
              <li class="active"><?php the_title(); ?></li>
            </ul>
          </div>
        </div>  

        <div class="row">
          <div class="col-xs-12 col-sm-8">
              <ol class="list-inline event-details-list">
                <?php if(get_post_meta( $post->ID, '_ctc_event_start_date', true) != get_post_meta( $post->ID, '_ctc_event_end_date', true)): ?>
                <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("M d", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?> - <?php echo date("M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_end_date', true))); ?></li>
                <?php else: ?>
                <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("D, M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?></li>
                <?php endif; ?>
                <?php if(get_post_meta( $post->ID, '_ctc_event_hide_time_range', true) == 1): ?>
                <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo get_post_meta( $post->ID, '_ctc_event_time', true); ?></li>
                <?php else: ?>
                <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_start_time', true))); ?> - <?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_end_time', true))); ?></li>
                <?php endif; ?>
                <?php if(!empty(get_post_meta( $post->ID, '_ctc_event_venue', true))): ?>
                <li><i class="event-detail-icon fa fa-fw fa-lg fa-location-arrow"></i><?php echo get_post_meta( $post->ID, '_ctc_event_venue', true); ?></li>
                <?php endif; ?>
                <li><i class="fa fa-2x fa-fw fa-refresh event-detail-icon"></i><?php echo ucfirst(get_post_meta( $post->ID, '_ctc_event_recurrence', true)); ?></li>
              </ol>
              <?php the_content(); ?>
              <div class="row">
                <?php if(!empty(get_post_meta( $post->ID, '_ctc_event_registration_url', true))): ?>
                <div class="col-xs-12 col-sm-6">
                    <p><a href="<?php echo get_post_meta( $post->ID, '_ctc_event_registration_url', true); ?>" class="btn btn-block btn-primary">Register for This Event</a></p>                
                </div>
                <?php endif; ?>
                <div class="col-xs-12 col-sm-6">
                  <?php do_action( 'addthis_widget', get_permalink(), get_the_title(), array(
                      'type' => 'custom',
                      'size' => '32', // size of the icons.  Either 16 or 32
                      'services' => 'facebook,twitter,linkedin', // the services you want to always appear
                      'preferred' => '3', // the number of auto personalized services
                      'more' => true, // if you want to have a more button at the end
                      'counter' => 'bubble_style' // if you want a counter and the style of it
                      ));
                  ?>
                    <!-- <p><a class="btn btn-block btn-primary">Share This Event</a></p> -->
                </div>
              </div>              
          </div>
          <div class="col-xs-12 col-sm-4">
            <?php if (has_post_thumbnail( $post->ID ) ):
              $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
              if($image[0]):
            ?>
              <p><img src="<?php echo $image[0]; ?>" class="img-responsive featured-image img-thumbnail" alt="<?php the_title(); ?>"></p>               
            <?php else: ?>
              <p><img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive featured-image img-thumbnail" alt="<?php the_title(); ?>"></p>   
            <?php endif; else: ?>
              <p><img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive featured-image img-thumbnail" alt="<?php the_title(); ?>"></p>   
            <?php endif; ?>         
            
            <?php if(get_post_meta( $post->ID, '_ctc_event_show_directions_link', true) == 1): ?>
            <p>
            <?php if ( tbcf_event_map() ) : ?>
              <div class="entry__map">
                <?php echo tbcf_event_map(); ?>
              </div>
            <?php else: ?>
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3159.184637626044!2d-121.0046524!3d37.644863!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x809053ee81c1ab1d%3A0xb9fe05439ab67f27!2sCrossPoint+Community+Church!5e0!3m2!1sen!2s!4v1484048659284" frameborder="0" style="border: 0;width: 100%;height: 100%;min-height: 250px;" allowfullscreen></iframe>
            <?php endif; ?>
            </p>          

            <p>
              <form action="http://maps.google.com/maps" method="get" target="_blank">
              <input type="hidden" name="saddr" />
              <?php if(!empty(get_post_meta( $post->ID, '_ctc_event_address', true))): ?>
                <input type="hidden" name="daddr" value="<?php echo get_post_meta( $post->ID, '_ctc_event_address', true); ?>" />
              <?php else: ?>
                <input type="hidden" name="daddr" value="CrossPoint Community Church, 1301 12th St, Modesto, CA 95354" />
              <?php endif; ?>
                <input type="submit" value="Get directions" class="btn btn-block btn-primary" />
              </form>
            </p>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

    <?php 
      $organizer = get_post( wpcf_pr_post_get_belongs( get_the_ID(), 'ctc_person' ) );
    ?>
      
    <!-- CONTACT SECTION BEGIN -->  
    <div class="section contact-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">              
              <h2 class="section-title section-title-white">EMAIL EVENT ORGANIZER</h2>
              <h4 style="color: #FFF;"><?php echo $organizer->post_title; ?> is the contact person.</h4>
              <?php echo do_shortcode( '[gravityform id="1" title="false" description="false" field_values=\'cp-person-name=' . $organizer->post_title . '&cp-person-email=' . get_post_meta( $organizer->ID, '_ctc_person_email', true) . '\']' ); ?>        
          </div>
        </div>
      </div>
    </div>

<?php endwhile; ?>

<?php
get_footer();