<?php
/**
 * The sidebar containing the footer widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package crosspoint
 */
?>


<?php if(is_active_sidebar( 'ministry-sidebar' )): ?>
<div class="col-xs-12 col-sm-12" style="padding: 0;">
	<?php dynamic_sidebar( 'ministry-sidebar' ); ?>         
</div>
<?php endif; ?>

