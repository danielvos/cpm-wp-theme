<?php
/**
 * The template for displaying persons by group.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

  <div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-12">
            <h1 class="page-title">Team: <?php echo single_cat_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>

    <div class="section section-archive">
      <div class="container">
        <div class="row">

          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ctc_person' ); ?>">People</a>
              </li> 
              <li class="active"><?php echo single_cat_title(); ?></li>
            </ul>
          </div>
        </div>

        <?php 
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          $custom_taxterm = get_queried_object()->term_id;

          $args = array(
            'post_type' => 'ctc_person',
            'post_status' => 'publish',
            'posts_per_page' => 9,
            'paged'  =>  $paged,
            'orderby'   => 'title',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'ctc_person_group',
                    'field' => 'id',
                    'terms' => $custom_taxterm
                )
            )
            );
          $items = new WP_Query( $args );
        ?>

        <div class="row">
        <?php if ( $items->have_posts() ) :
          while ( $items->have_posts() ) : $items->the_post(); ?>
            <div class="col-xs-12 col-sm-4 col-md-offset-0 blog-post-container">
              <div class="col-xs-12 blog-post" style="min-height: 350px;">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" /></a>                  
                <?php else: ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; else: ?>
                <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; ?>
                <div class="col-xs-12 blog-content">
                  <h3><a href="<?php the_permalink() ?>" class="blog-post-title"><?php the_title(); ?></a></h3>
                  <?php echo get_post_meta( $post->ID, '_ctc_person_position', true); ?>
                </div>
              </div>
            </div>
        <?php endwhile; ?>
        </div>       

        <div class="row">
          <div class="col-md-12">
            <?php numeric_posts_navigation(); ?>
          </div>
        </div>
      <?php else: ?>
        <div class="row">
            <div class="col-md-12">
              <h3>Nothing Found!</h3>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>

<?php
get_footer();