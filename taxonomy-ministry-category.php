<?php
/**
 * The template for displaying single speaker.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

  <div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-12">
            <h1 class="page-title"><?php echo single_cat_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>

    <div class="section section-archive">
      <div class="container">
        <div class="row">

          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ministry' ); ?>">Ministries</a>
              </li>
              <li class="active"><?php echo single_cat_title(); ?></li>
            </ul>
          </div>
        </div>

        <?php 
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          $custom_taxterms = wp_get_object_terms( $post->ID, 'ministry-category', array('fields' => 'ids') );

          $args = array(
            'post_type' => 'ministry',
            'post_status' => 'publish',
            'posts_per_page' => 9,
            'paged'  =>  $paged,
            'orderby'   => 'title',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'ministry-category',
                    'field' => 'id',
                    'terms' => $custom_taxterms
                )
            )
            );
          $items = new WP_Query( $args );
          //print_r($items->posts);
        ?>
        
        <div class="row">
        <?php if ( $items->have_posts() ) :
          while ( $items->have_posts() ) : $items->the_post(); ?>
            <div class="col-xs-12 col-sm-4 col-md-offset-0 blog-post-container">
              <div class="col-xs-12 blog-post" style="min-height: inherit;">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" /></a>                  
                <?php else: ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; else: ?>
                <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; ?>
                <div class="col-xs-12 blog-content">
                  <h3><a href="<?php the_permalink() ?>" class="blog-post-title"><?php the_title(); ?></a></h3>
                </div>
              </div>
            </div>
    <?php endwhile; ?>
        </div>       

        <div class="row">
          <div class="col-md-12">
            <?php numeric_posts_navigation(); ?>
          </div>
        </div>
      <?php else: ?>
        <div class="row">
            <div class="col-md-12">
              <h3>Nothing Found!</h3>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>

<?php
get_footer();