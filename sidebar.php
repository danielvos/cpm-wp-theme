<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package crosspoint
 */
?>

<?php if(is_active_sidebar( 'right-sidebar' )): ?>
<div class="col-xs-12 col-sm-4">
	<?php dynamic_sidebar( 'right-sidebar' ); ?>         
</div>
<?php endif; ?>