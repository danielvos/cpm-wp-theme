<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-9">
            <h1 class="page-title">Post Archive</h1>
          </div>
          <div class="col-xs-12 col-md-3 sort-buttons">
          	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	            <div class="input-group input-group-sm">
	              <span class="input-group-btn">
	                <button class="btn btn-default btn-lg" type="submit">
	                  <i class="fa fa-fw fa-search"></i>
	                </button>
	              </span>
	              <input type="text" name="s" id="s" class="form-control" placeholder="Search by Keyword" />
	            </div>
	        </form>
          </div>
        </div>
      </div>
    </div>

    <div class="section section-archive">
      <div class="container">
        <div class="row">

          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="active">Post Archive</li>
            </ul>
          </div>
        </div>

        <div class="row">
        <?php if ( have_posts() ) :
        	while ( have_posts() ) : the_post(); ?>
            <div class="col-xs-12 col-sm-4 col-md-offset-0 blog-post-container">
              <div class="col-xs-12 blog-post">
              	<?php if (has_post_thumbnail( $post->ID ) ):
            		  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>
            		  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" /></a>                  
				        <?php else: ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; else: ?>
                <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; ?>
                <div class="col-xs-12 blog-content">
                  <h3><a href="<?php the_permalink() ?>" class="blog-post-title"><?php the_title(); ?></a></h3>
                  <?php the_excerpt(); ?>
                </div>
              </div>
            </div>
		      <?php endwhile; ?>
        </div>       

        <div class="row">
          <div class="col-md-12">
          	<?php numeric_posts_navigation(); ?>
          </div>
        </div>
        <?php endif; ?>
      </div>
    </div>

<?php
get_footer();
