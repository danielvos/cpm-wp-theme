<?php
/**
 * The template for displaying search results of event.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package crosspoint
 */

get_header(); ?>

  <?php if(!empty($_GET['category'])): ?>
    <div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <h1 class="page-title">UPCOMING EVENTS -  
            <?php 
              $category = get_term_by('slug', $_GET['category'], 'ctc_event_category'); 
              echo $category->name;
            ?>
            </h1>
          </div>    
        </div>
      </div>
    </div>
  <?php else: ?>
	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-9">
            <h1 class="page-title"><?php printf( esc_html__( 'Search Results for "%s"', 'crosspoint' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
          </div>
        </div>
      </div>
    </div>    
  <?php endif; ?>

    <!-- EVENT SECTION BEGIN -->  
    <div class="section event-section ">
      <div class="container">
       <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ctc_event' ); ?>">Calendar</a>
              </li>  
              <li class="active"><?php printf( esc_html__( 'Search Results for %s', 'crosspoint' ), '<span>' . get_search_query() . '</span>' ); ?></li>
            </ul>
          </div>
        </div>  

      <?php if(!empty($_GET['category'])): ?>
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ctc_event' ); ?>">Calendar</a>
              </li>  
              <li class="active">Upcoming Events - 
                <?php 
                  $category = get_term_by('slug', $_GET['category'], 'ctc_event_category'); 
                  echo $category->name;
                ?>                
              </li>
            </ul>
          </div>
        </div>  
      <?php endif; ?>

      <?php 
        $events_args = array(
          'post_type'     => 'ctc_event',                
          'meta_query' => array(
            array(
              'key' => '_ctc_event_end_date',
              'value' => date_i18n( 'Y-m-d' ),
              'compare' => '>=',
              'type' => 'DATE'
            ),
          ),
          'meta_key'      => '_ctc_event_start_date_start_time',
          'meta_type'     => 'DATETIME',
          'orderby'     => 'meta_value',
          'order'       => 'ASC',
          's'    =>  get_search_query(), 
        );

        $events = new WP_Query( $events_args ); 

        if ( $events->have_posts() ) : ?>
        <div class="row">
          <div class="col-xs-12 event-container">
          <?php  while ( $events->have_posts() ) : $events->the_post(); ?>
            <div class="row event-row">
              <div class="col-xs-12 col-sm-8">
                <a href="<?php the_permalink(); ?>"><h3 class="event-title"><?php the_title(); ?></h3></a>
                <ol class="list-inline event-details-list">
                  <?php if(get_post_meta( $post->ID, '_ctc_event_start_date', true) != get_post_meta( $post->ID, '_ctc_event_end_date', true)): ?>
                  <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("M d", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?> - <?php echo date("M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_end_date', true))); ?></li>
                  <?php else: ?>
                  <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("D, M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?></li>
                  <?php endif; ?>
                  <?php if(get_post_meta( $post->ID, '_ctc_event_hide_time_range', true) == 1): ?>
                  <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo get_post_meta( $post->ID, '_ctc_event_time', true); ?></li>
                  <?php else: ?>
                  <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_start_time', true))); ?> - <?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_end_time', true))); ?></li>
                  <?php endif; ?>
                  <?php if(!empty(get_post_meta( $post->ID, '_ctc_event_venue', true))): ?>
                  <li><i class="event-detail-icon fa fa-fw fa-lg fa-location-arrow"></i><?php echo get_post_meta( $post->ID, '_ctc_event_venue', true); ?></li>
                  <?php endif; ?>
                  <li><i class="fa fa-2x fa-fw fa-refresh event-detail-icon"></i><?php echo ucfirst(get_post_meta( $post->ID, '_ctc_event_recurrence', true)); ?></li>
                </ol>
                <?php the_excerpt(); ?>
              </div>
              <div class="hidden-xs col-sm-12 col-sm-4 event-details-image-container">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>      
                  <img src="<?php echo $image[0]; ?>" class="event-details-image img-responsive" alt="<?php the_title(); ?>">        
                <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="event-details-image img-responsive" alt="<?php the_title(); ?>"> 
                <?php endif; else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="event-details-image img-responsive" alt="<?php the_title(); ?>"> 
                <?php endif; ?>   
              </div>
            </div>
            <?php endwhile; ?>
          </div>
        </div>

        <?php else: ?>
          <div class="row">
            <div class="col-md-12">
              <h3>Nothing Found!</h3>
            </div>
          </div>
        <?php endif; ?>    
          
      </div>
    </div>
    <!-- EVENT SECTION END --> 

<?php
get_footer();
