<?php
/**
 * Template Name: Posts
 *
 * This is the template that displays all message series.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

get_header(); ?>

  <div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-9">
            <h1 class="page-title">Post Archive</h1>
          </div>
          <div class="col-xs-12 col-md-3 sort-buttons">
            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
              <div class="input-group input-group-sm">
                <span class="input-group-btn">
                  <button class="btn btn-default btn-lg" type="submit">
                    <i class="fa fa-fw fa-search"></i>
                  </button>
                </span>
                <input type="text" name="s" id="s" class="form-control" placeholder="Search by Keyword" />
              </div>
          </form>
          </div>
        </div>
      </div>
    </div>

    <div class="section section-archive">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="active">Post Archive</li>
            </ul>
          </div>
        </div>

        <div class="row">
        <?php 
            $args = array(
                'post_type' => 'post',
                'post_status' => array( 'publish' ),
                'posts_per_page'  => 9,
                'paged' => $paged,
                'order' => 'DESC',
                'orderby' => 'ID',
                'ignore_sticky_posts' => true,
            );
            $posts = new WP_Query( $args );

            $temp_query = $wp_query;
            $wp_query   = NULL;
            $wp_query   = $posts;

        if ( $posts->have_posts() ) :
          while ( $posts->have_posts() ) : $posts->the_post(); ?>
            <div class="col-xs-12 col-sm-4 col-md-offset-0 blog-post-container">
              <div class="col-xs-12 blog-post">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" /></a>                  
                <?php else: ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; else: ?>
                <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; ?>
                <div class="col-xs-12 blog-content">
                  <h3><a href="<?php the_permalink() ?>" class="blog-post-title"><?php the_title(); ?></a></h3>
                  <?php the_excerpt(); ?>
                </div>
              </div>
            </div>
          <?php endwhile; ?>
        </div>       

        <div class="row">
          <div class="col-md-12">
            <?php numeric_posts_navigation(); ?>
          </div>
        </div>
        <?php endif; ?>
      </div>
    </div>

<?php
get_footer();
