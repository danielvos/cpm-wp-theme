<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package crosspoint
 */

get_header(); ?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-9">
            <h1 class="page-title">404, Page not Found</h1>
          </div>
          <div class="col-xs-12 col-md-3 sort-buttons">
          </div>
        </div>
      </div>
    </div>

    <div class="section section-archive">
      <div class="container">

        <div class="row">
        	<div class="col-sm-12">
        		<h3>It looks like nothing was found at this location. Maybe try a search?</h3>
        	</div>

        	<p>&nbsp;</p>

        	<div class="col-sm-4">
        		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		            <div class="input-group input-group-sm">
		              <span class="input-group-btn">
		                <button class="btn btn-default btn-lg" type="submit">
		                  <i class="fa fa-fw fa-search"></i>
		                </button>
		              </span>
		              <input type="text" name="s" id="s" class="form-control" placeholder="Search by Keyword" />
		            </div>
		        </form>
        	</div>
        </div>       
      </div>
    </div>

<?php
get_footer();
