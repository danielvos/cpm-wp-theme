<?php
/**
 * The template for displaying single speaker.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

<?php 
  $series = get_the_terms( $post->ID, 'ctc_sermon_series' ); 
  $speaker = get_the_terms( $post->ID, 'ctc_sermon_speaker' ); 
?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <h1 class="page-title">Messages</h1>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 text-right sort-buttons">
            <div class="btn-group btn-group-sm">
              <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Browse By Series   <span class="fa fa-caret-down"></span></a>
            	<?php
	              $series_category = get_terms( 'ctc_sermon_series' );
	              if ( ! empty( $series_category ) && ! is_wp_error( $series_category ) ){
	                echo '<ul class="dropdown-menu" role="menu">';
	                foreach ( $series_category as $secat ) {                      
	                  echo '<li><a href="' . get_term_link( $secat->slug, 'ctc_sermon_series' ) . '">' . $secat->name . '</a></li>';
	                }
	               echo '</ul>';
	              }
	        	?>
            </div>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 text-right sort-buttons">
            <div class="btn-group btn-group-sm">
              <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Browse By Speaker   <span class="fa fa-caret-down"></span></a>
            	<?php
	              $speaker_category = get_terms( 'ctc_sermon_speaker' );
	              if ( ! empty( $speaker_category ) && ! is_wp_error( $speaker_category ) ){
	                echo '<ul class="dropdown-menu" role="menu">';
	                foreach ( $speaker_category as $scat ) {                      
	                  echo '<li><a href="' . get_term_link( $scat->slug, 'ctc_sermon_speaker' ) . '">' . $scat->name . '</a></li>';
	                }
	               echo '</ul>';
	              }
	        	?>
            </div>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 sort-buttons">
            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                <input type="hidden" name="search_type" value="messages" />
                <div class="input-group input-group-sm">
                  <input type="text" name="s" id="s" class="form-control" placeholder="Search Messages" />
                </div>
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ctc_sermon' ); ?>">Messages</a>
              </li>                      
              <li class="active">Messages by <?php echo $speaker[0]->name; ?></li>
            </ul>
          </div>
        </div>    
        <div class="row">    
          <!-- MAIN CONTENT AREA -->
        <div class="col-xs-12 col-sm-8">
          <h2 class="section-title">Messages by <?php echo $speaker[0]->name; ?></h2>    
          <ul class="list-group message-list">
          	<?php 
	            $custom_taxterms = wp_get_object_terms( $post->ID, 'ctc_sermon_speaker', array('fields' => 'ids') );
	            $args = array(
	              'post_type' => 'ctc_sermon',
	              'post_status' => 'publish',
	              'posts_per_page' => -1,
	              'orderby'   => 'id',
	              'order' => 'DESC',
	              'tax_query' => array(
	                  array(
	                      'taxonomy' => 'ctc_sermon_speaker',
	                      'field' => 'id',
	                      'terms' => $custom_taxterms
	                  )
	              ),
	             );
	            $related_items = new WP_Query( $args );

	            if ($related_items->have_posts()) :
	              echo '<ul class="list-group message-list">';
	              while ( $related_items->have_posts() ) : $related_items->the_post();
	                $rp_speaker = get_the_terms( $post->ID, 'ctc_sermon_speaker' ); 
	          ?>
	                <li class="list-group-item">
	                  <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	                  <span><i class="fa fa-calendar fa-fw"></i> <?php echo get_the_date('F j, Y'); ?></span>
	                  <span><i class="fa fa-fw fa-microphone"></i><?php echo $rp_speaker[0]->name; ?></span>
	                </li>
	          <?php
	                endwhile;
	              echo '</ul>';
	            endif;

	            wp_reset_postdata();
	          ?>            
          </ul>
        </div>
          
          <!-- SIDEBAR -->
          <div class="col-xs-12 col-sm-4 ">
            <?php 
            if(!empty(types_render_termmeta( "message-speaker-image", array("term_id" => $speaker[0]->term_id)))): 
              echo types_render_termmeta( "message-speaker-image", array("term_id" => $speaker[0]->term_id, "alt" => $speaker[0]->name, "class" => "img-responsive") );
            ?>
            <?php else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive" alt="<?php echo $speaker[0]->name; ?>"> 
            <?php endif; ?>
            <strong class="big-text black">Speaker:</strong>
            <h3 class="margin-bottom-10"><?php echo $speaker[0]->name; ?></h3>
            <p><?php echo $speaker[0]->description; ?></p>            
          </div>
            
        </div>
      </div>
    </div>    

<?php
get_footer();