<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="page-title"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="active"><?php the_title(); ?></li>
            </ul>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12 col-sm-8">
            <?php the_content(); ?>
          </div>
          <?php get_sidebar(); ?>          
        </div>
      </div>
    </div>

    <?php comments_template(); ?> 

<?php endwhile; ?>

<?php
get_footer();
