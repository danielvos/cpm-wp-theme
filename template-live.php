<?php
/**
 * Template Name: Livestream
 *
 * This is the template that displays all message series.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>


<div class="page-title-section section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1 class="page-title"><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
</div>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php
          date_default_timezone_set("America/Los_Angeles");
          $current_time = date('h:i a');

          $start_hour = 8;
          $start_minute = 30;

          $end_hour = 12;
          $end_minute = 20;

          $day = "Sun";

          /*if (isset($_GET["custom_time"]))
          { //uncomment this to test times
                if (isset($_GET["sh"]))
                    $start_hour = $_GET["sh"];

                if (isset($_GET["sm"]))
                    $start_minute = $_GET["sm"];

                if (isset($_GET["eh"]))
                    $end_hour = $_GET["eh"];

                if (isset($_GET["em"]))
                    $end_minute = $_GET["em"];

                if (isset($_GET["day"]))
                    $day = $_GET["day"];
          }*/

          $start_time = "$start_hour:$start_minute am";
          $end_time = "$end_hour:$end_minute pm";

          $ctime = DateTime::createFromFormat('H:i a', $current_time);
          $stime = DateTime::createFromFormat('H:i a', $start_time);
          $etime = DateTime::createFromFormat('H:i a', $end_time);

          if(date('D') == $day){
            if ($ctime >= $stime && $ctime <= $etime) {
                echo types_render_field( "livestream-video-embed-code", array( "output" => "raw" ) );
            }
            else {
                echo types_render_field( "placeholder-video-embed-code", array( "output" => "raw" ) );
              ?>
              <script type="text/javascript">
              //Don't need to check to see if it's the right day - already done by the php code
              setInterval(checkReset, 1000 * 60);
              function checkReset()
              {
                  var startTime = new Date().setHours( <? echo $start_hour . ", " . $start_minute ?>, 0, 0);
                  var endTime = new Date().setHours( <? echo $end_hour . ", " . $end_minute ?>, 0, 0);
                  var current = new Date();

                  var diffStart = startTime - current.GetTime();
                  var diffEnd = endTime - current.GetTime();

                  if (diffEnd > 0 && diffStart <= 0) { //should it be greater than 0? test.
                      location.reload();
                  }
              }
              </script>
              <?
            }
          }
          else{
            echo types_render_field( "placeholder-video-embed-code", array( "output" => "raw" ) );
          }
        ?>
        <?php //echo date('D - d-m-Y - h:i:s A'); //the_content(); ?>
      </div>
    </div>

    <div class="row" style="width: 80%; margin: 1em auto;">
       <div class="col-sm-4 live-btn" style="margin-bottom: 1em;">
        <a href="https://cpmodesto.org/ministry-category/life-stage/" class="btn btn-primary" style="width: 100%;">Get Connected</a>
      </div>
       <div class="col-sm-4 live-btn" style="margin-bottom: 1em;">
        <a href="https://cpmodesto.org/first-visit/" class="btn btn-primary" style="width: 100%;">Plan My Visit</a>
      </div>
      <div class="col-sm-4 live-btn">
        <a href="https://cpmodesto.org/pray/" class="btn btn-primary" style="width: 100%;">I Need Prayer</a>
      </div>
    </div>
  </div>
</div>

<div class="section" style="padding-top: 0px; padding-bottom: 0px;">
  <div class="container">
     <?php while(have_posts()) : the_post(); ?>
     <!-- Content from Page Bulider -->
     <?php the_content(); ?>
     <?php endwhile; ?>
  </div>
</div>

<div class="section message-series-section">
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h2 class="section-title">MESSAGES BY SERIES</h2>
    </div>
  </div>

  <div class="row">
    <?php
      $results = get_series_categories();
      $catsArray = [];
      foreach($results as $key => $res){
        $catsArray[] = $key;
      }

      $mseries = get_terms( array(
        'taxonomy' => 'ctc_sermon_series',
        'number'  =>  6,
        'include' => $catsArray,
        'orderby'  => 'include',
      ) );

      $count = 1;
      foreach($mseries as $ser):
    ?>
      <div class="col-sm-4 series-box"<?php echo (($count == 4) ? ' style="clear: left;"' : ''); ?>>
        <a href="<?php echo get_term_link( $ser->slug, 'ctc_sermon_series' ) ?>">
        <div class="series-box-container">
          <?php if(!empty(types_render_termmeta( "message-series-image", array("term_id" => $ser->term_id)))):
            echo types_render_termmeta( "message-series-image", array("term_id" => $ser->term_id, "alt" => $ser->name, "class" => "img-responsive") );
          ?>
          <?php else: ?>
            <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive" alt="<?php echo $ser->name; ?>">
          <?php endif; ?>
          <div class="box-content">
            <h5><?php echo $ser->name; ?></h5>
          </div>
        </div>
        </a>
      </div>
    <?php $count++; endforeach; ?>
  </div>

  <div class="row">
      <div class="col-xs-12 text-center">
        <a href="<?php echo get_permalink( get_page_by_path( 'sermon-series' ) ); ?>" class="btn btn-primary">View All Series</a>
      </div>
  </div>
</div>

<?php comments_template(); ?>

<?php
get_footer();
