<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package crosspoint
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" type="image/png" href="<?php echo cs_get_option( 'favicon' ); ?>" />

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<div class="navbar navbar-default navbar-static-top nav-bar-main">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo home_url( '/' ); ?>" class="navbar-brand logo-container"><img height="60" alt="<?php bloginfo( 'name' ); ?>" src="<?php echo cs_get_option( 'logo' ); ?>"></a>
        </div>

        <?php
            wp_nav_menu( array(
                'menu'  => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_id'      => 'navbar-ex-collapse',
                'container_class'   => 'collapse navbar-collapse',
                'menu_class'        => 'nav navbar-nav navbar-right navbar-menu',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>

      </div>
    </div>
    <div class="section page-header">
      <div class="background-image"></div>
    </div>	

	<div id="content" class="site-content">
