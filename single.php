<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package crosspoint
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="page-title"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>

    <div class="section">
      <div class="container">

        <div class="row">
          <div class="col-sm-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb"><a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a></li>
              <li><a href="<?php echo get_permalink( get_page_by_title( 'Posts' ) ); ?>">Posts</a></li>
              <li class="active"><?php the_title(); ?></li>
            </ul>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12 col-sm-8">
            <ol class="list-inline metadata-list blog-metadata">
              <li>
                <i class="fa fa-fw fa-lg fa-user"></i> <?php echo ucfirst(get_the_author()); ?>
              </li>
              <li>
                <i class="fa fa-calendar fa-fw fa-lg"></i> <?php the_date(); ?>
              </li>
            </ol>              
            <?php the_content(); ?>
          </div>

          <?php get_sidebar(); ?>    
        </div>
        </div>
    </div>

    <?php comments_template(); ?> 

    <?php 
      $related_post = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );        
        if($related_post):
    ?>
    <!-- BLOG POST SECTION BEGIN -->  
    <div class="section blog-post-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">
            <h2 class="section-title">Related Posts</h2>
          </div>
        </div>
        <div class="row">
      <?php				
					foreach($related_post as $post): setup_postdata($post); 
			?>
			<div class="col-xs-12 col-sm-4 col-md-offset-0 blog-post-container">
              <div class="col-xs-12 blog-post">
            	<?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>
                  <img class="img-responsive blog-featured-image" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" />
                <?php else: ?>
                  <img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" />
                <?php endif; else: ?>
                  <img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" />
                <?php endif; ?>
                
                <div class="col-xs-12 blog-content">
                  <h3><a href="<?php the_permalink() ?>" class="blog-post-title"><?php the_title(); ?></a></h3>
                  <?php the_excerpt(); ?>
                </div>
              </div>
            </div>
			<?php endforeach; ?>            
        </div>
      </div>
    </div>
    <!-- BLOG POST SECTION END --> 
    <?php 
      endif;
      wp_reset_postdata(); 
    ?>

<?php endwhile; ?>

<?php
get_sidebar();
get_footer();
