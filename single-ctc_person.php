<?php
/**
 * The template for displaying single person.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php 
  $fullname = get_the_title();
  $email = get_post_meta( $post->ID, '_ctc_person_email', true);
  $name_array = explode(' ', get_the_title()); 
?>

    <div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="page-title"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="breadcrumb">
                <a href="<?php echo get_post_type_archive_link( 'ctc_person' ); ?>">People</a>
              </li>  
              <li class="active"><?php the_title(); ?></li>
            </ul>
          </div>
        </div>  

        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-8">
            <h2 class="section-title"><?php echo get_post_meta( $post->ID, '_ctc_person_position', true); ?></h2>
            <ol class="list-inline event-details-list">
            <?php if(!empty(get_post_meta( $post->ID, '_ctc_person_phone', true))): ?>
              <li><i class="event-detail-icon fa fa-fw fa-lg fa-phone"></i><?php echo get_post_meta( $post->ID, '_ctc_person_phone', true); ?></li>
            <?php endif; ?>
              <li><a href="#contact-person"><i class="event-detail-icon fa fa-2x fa-fw fa-envelope"></i></a></li>
              <?php 
                $urls = preg_split ('/$\R?^/m', get_post_meta( $post->ID, '_ctc_person_urls', true));
                foreach($urls as $url){
                  if(strpos($url, 'facebook') !== false){
                    echo '<li><a href="' . $url . '" target="_blank"><i class="event-detail-icon fa fa-fw fa-lg fa-facebook-square"></i></a></li>';
                  }
                  elseif(strpos($url, 'twitter') !== false){
                    echo '<li><a href="' . $url . '" target="_blank"><i class="event-detail-icon fa fa-fw fa-lg fa-twitter-square"></i></a></li>';
                  }
                  elseif(strpos($url, 'instagram') !== false){
                    echo '<li><a href="' . $url . '" target="_blank"><i class="event-detail-icon fa fa-fw fa-lg fa-instagram"></i></a></li>';
                  }
                  elseif(filter_var($url, FILTER_VALIDATE_EMAIL)){
                    echo '<li><a href="mailto:' . $url . '"><i class="event-detail-icon fa fa-fw fa-lg fa-envelope-square"></i></a></li>';
                  }
                  else{
                    echo '';
                  }
                }
              ?>
            </ol>

            <?php the_content(); ?>

            <div class="btn-group person-socials-borderless">
              <p>
              
              </p>
            </div>  
              
          </div>
          <div class="col-xs-12 col-sm-5 col-md-4 ministry-image-container">
            <?php if (has_post_thumbnail( $post->ID ) ):
              $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
              if($image[0]):
            ?>    
              <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" class="center-block img-responsive ministry-image">            
            <?php else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="center-block img-responsive ministry-image" alt="<?php the_title(); ?>" />
            <?php endif; else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="center-block img-responsive ministry-image" alt="<?php the_title(); ?>" />
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>

  <?php if(!empty( get_post_meta( $post->ID, 'wpcf-quote-by-person', true) )): ?>
    <div class="section">
      <?php if(!empty(get_post_meta( $post->ID, 'wpcf-quote-background-image', true))): ?>
        <div class="background-image" style="background-image : url('<?php echo get_post_meta( $post->ID, "wpcf-quote-background-image", true); ?>')"></div>
      <?php else: ?>
        <div class="background-image" style="background-image : url('<?php echo get_template_directory_uri() . "/images/header.jpg"; ?>')"></div>
      <?php endif; ?>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 col-xs-12 text-center">
            <i class="-official -square fa fa-3x fa-fw fa-quote-right white"></i>
            <h3 class="white"><?php echo get_post_meta( $post->ID, 'wpcf-quote-by-person', true); ?></h3>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php 
    $custom_taxterms = wp_get_object_terms( $post->ID, 'ministry-category', array('fields' => 'ids') );
    $args = array(
      'post_type' => 'ministry',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'orderby'   => 'title',
      'order' => 'ASC',
      'tax_query' => array(
          array(
              'taxonomy' => 'ministry-category',
              'field' => 'id',
              'terms' => $custom_taxterms
          )
      ),
      'post__not_in' => array ($post->ID),
      );
    $related_items = new WP_Query( $args );
    if ($related_items->have_posts()) :
  ?>
    <!-- STAFF SECTION BEGIN -->  
    <div class="section staff-section" id="staff-carousel">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="section-title-white">Other People on This Team</h2>
          </div>

          <div class="col-xs-12 carouFredSel-wrap white-bg">
            <div class="carouFredSel preSlider">

              <?php                 
                  while ( $related_items->have_posts() ) : $related_items->the_post();
              ?>             
                  <div class="item">
                    <a href="<?php the_permalink(); ?>">
                      <?php if (has_post_thumbnail( $related_items->ID ) ):
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $related_items->ID ), 'featured-image' );
                        if($image[0]):
                      ?>    
                        <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" class="center-block img-responsive ministry-image">            
                      <?php else: ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive staff-image" alt="<?php the_title(); ?>" />
                      <?php endif; else: ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive staff-image" alt="<?php the_title(); ?>" />
                      <?php endif; ?>
                    </a>
                    <div class="slide-details">
                      <a href="<?php the_permalink(); ?>"><h3 class="staff-name"><?php the_title(); ?></h3></a>
                      <p><?php echo get_post_meta( $post->ID, '_ctc_person_position', true); ?></p>
                    </div>
                  </div>
                <?php endwhile; ?>          
            </div>
            <span class="fa fa-chevron-right slide-next"></span>
          </div>          
        </div>
      </div>
    </div>
    <!-- STAFF SECTION END -->    
  <?php endif; wp_reset_query(); ?>
  
    <div id="contact-person" class="section contact-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <h2 class="section-title-white">CONTACT <?php echo $name_array[0]; ?></h2>
            <?php echo do_shortcode( '[gravityform id="1" title="false" description="false" field_values=\'cp-person-name=' . $fullname . '&cp-person-email=' . $email . '\']' ); ?>
          </div>
        </div>
      </div>
    </div>


<?php endwhile; ?>

<?php
get_footer();