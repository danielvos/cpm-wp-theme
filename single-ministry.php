<?php
/**
 * The template for displaying single ministry.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php $mtitle = get_the_title(); ?>

  <!-- PAGE TITLE SECTION BEGIN -->  
    <div class="section page-title-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">
            <h1 class="page-title"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>
    <!-- PAGE TITLE SECTION END -->  

	<!-- PAGE CONTENT SECTION BEGIN -->  
    <div class="section page-content-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li>
                <a href="<?php echo get_post_type_archive_link( 'ministry' ); ?>">Ministries</a>
              </li>
              <li class="active"><?php the_title(); ?></li>
            </ul>

            <div class="row">
              <div class="col-xs-12 col-sm-8">
                <?php the_content(); ?>
              </div>
              <div class="col-xs-12 col-sm-4">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>    
                  <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive featured-image img-thumbnail">            
                <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive featured-image img-thumbnail" alt="<?php the_title(); ?>" />
                <?php endif; else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive featured-image img-thumbnail" alt="<?php the_title(); ?>" />
                <?php endif; ?>

                <?php 
                  $child_args = array(
                    'post_type'      => 'ministry',
                    'posts_per_page' => -1,
                    'post_parent'    => $post->ID,
                    'orderby'   => 'id',
                    'order' => 'DESC',
                 );

                $childs = new WP_Query( $child_args );
                 if ( $childs->have_posts() ) : ?>
                  <h3>More for You</h3>
                  <?php while ( $childs->have_posts() ) : $childs->the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="btn btn-default btn-primary btn-block"><?php the_title(); ?></a>
                  <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>

              <?php get_sidebar( 'ministry' ); ?>
              </div>                
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- PAGE CONTENT SECTION END -->  

    <?php 
      $child_events = types_child_posts('ministry-event'); 
      if(!empty( $child_events )):
    ?>    
    <!-- EVENT SECTION BEGIN -->  
    <div class="section event-section ">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 event-container">
            <h2 class="section-title">UPCOMING EVENTS</h2>
            <?php             
              $events_ids = [];                      
              foreach ($child_events as $cevent):             
                $event_id = wpcf_pr_post_get_belongs($cevent->ID, 'ctc_event');      
                $events_ids[] = wpcf_pr_post_get_belongs($cevent->ID, 'ctc_event');               
              endforeach;

              $ev_args = array(
                'post_type'     => 'ctc_event',                
                'meta_query' => array(
                  array(
                    'key' => '_ctc_event_end_date',
                    'value' => date_i18n( 'Y-m-d' ),
                    'compare' => '>=',
                    'type' => 'DATE'
                  ),
                ),
                'meta_key'      => '_ctc_event_start_date_start_time',
                'meta_type'     => 'DATETIME',
                'orderby'     => 'meta_value',
                'order'       => 'ASC',
                'post__in' => $events_ids,
              );
              $ev_event = new WP_Query( $ev_args );  

              $ev1 = [];
              $ev2 = []; 
              while ( $ev_event->have_posts() ) : $ev_event->the_post();
                if( has_term( 'featured', 'ctc_event_category' ) ) {
                  $ev1[] = $post->ID;
                }
                else{
                  $ev2[] = $post->ID;
                }
              endwhile;   
              $ev3 = array_merge($ev1, $ev2);
              wp_reset_query();

              if(count($ev3) > 0):
              $events_args = array(
                'post_type'     => 'ctc_event',
                'post__in' => $ev3,
                'orderby' => 'post__in',
              );
              $event = new WP_Query( $events_args );  

              while ( $event->have_posts() ) : $event->the_post();
            ?>
            <div class="row event-row">
              <div class="col-xs-12 col-sm-8">
                <a href="<?php echo get_permalink( $post->ID ); ?>"><h3 class="event-title"><?php the_title(); ?></h3></a>
                <ol class="list-inline event-details-list">
                  <?php if(get_post_meta( $post->ID, '_ctc_event_start_date', true) != get_post_meta( $post->ID, '_ctc_event_end_date', true)): ?>
                  <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("M d", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?> - <?php echo date("M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_end_date', true))); ?></li>
                  <?php else: ?>
                  <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("D, M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?></li>
                  <?php endif; ?>
                  <?php if(get_post_meta( $post->ID, '_ctc_event_hide_time_range', true) == 1): ?>
                  <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo get_post_meta( $post->ID, '_ctc_event_time', true); ?></li>
                  <?php else: ?>
                  <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_start_time', true))); ?> - <?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_end_time', true))); ?></li>
                  <?php endif; ?>
                  <?php if(!empty(get_post_meta( $post->ID, '_ctc_event_venue', true))): ?>
                  <li><i class="event-detail-icon fa fa-fw fa-lg fa-location-arrow"></i><?php echo get_post_meta( $post->ID, '_ctc_event_venue', true); ?></li>
                  <?php endif; ?>
                  <li><i class="fa fa-2x fa-fw fa-refresh event-detail-icon"></i><?php echo ucfirst(get_post_meta( $post->ID, '_ctc_event_recurrence', true)); ?></li>
                  <?php if(has_term('featured', 'ctc_event_category')): ?>
                  <li><i class="fa fa-2x fa-fw fa-star event-detail-icon"></i>Featured</li>
                  <?php endif; ?>
                </ol>
                <p><?php the_excerpt(); ?></p>
              </div>
              <div class="hidden-xs col-sm-12 col-sm-4 event-details-image-container">
                <a href="<?php echo get_permalink( $post->ID ); ?>">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>
                  <img src="<?php echo $image[0]; ?>" class="img-responsive featured-image img-thumbnail" alt="<?php echo $event->post_title; ?>">    
                <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="event-details-image img-responsive" alt="<?php echo $event->post_title; ?>">
                <?php endif; else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="event-details-image img-responsive" alt="<?php echo $event->post_title; ?>"> 
                <?php endif; ?>  
                </a>
              </div>
            </div>
            <?php endwhile; endif; ?>
          </div>
        </div>
      </div>
    </div>
    <!-- EVENT SECTION END -->  
    <?php endif; wp_reset_query(); ?>

    <?php 
      $ministry_leaders = types_child_posts('ministry-leader-role');     

      $leaders_ids = [];
      foreach ($ministry_leaders as $mleader):
        $leaders_ids[] = wpcf_pr_post_get_belongs($mleader->ID, 'ctc_person'); 
      endforeach; 

      $leader_args = array(
          'post_type'      => 'ctc_person',
          'posts_per_page' => -1,
          'orderby'   => 'title',
          'order' => 'ASC',
          'post__in' => $leaders_ids
       );
      $child_leaders = new WP_Query( $leader_args );
      
      if( $child_leaders->have_posts() ):
    ?>       
    <!-- STAFF SECTION BEGIN -->  
    <div class="section staff-section" id="staff-carousel">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="section-title section-title-white">Leaders</h2>
          </div>

          <div class="col-xs-12 carouFredSel-wrap white-bg">
            <div class="carouFredSel preSlider">
            <?php 
              while ( $child_leaders->have_posts() ) : $child_leaders->the_post();
            ?>
              <div class="item">
                <a href="<?php the_permalink(); ?>">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>
                  <p><img src="<?php echo $image[0]; ?>" class="img-responsive staff-image" alt="<?php the_title(); ?>"></p>               
                <?php else: ?>
                  <p><img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive staff-image" alt="<?php the_title(); ?>"></p>   
                <?php endif; else: ?>
                  <p><img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive staff-image" alt="<?php the_title(); ?>"></p>   
                <?php endif; ?>  
                </a>
                <div class="slide-details">
                  <a href="<?php the_permalink(); ?>"><h3 class="staff-name"><?php the_title(); ?></h3></a>
                  <p><?php echo get_post_meta( $post->ID, '_ctc_person_position', true); ?></p>
                </div>
              </div>
            <?php endwhile; ?>
            </div>
            <span class="fa fa-chevron-right slide-next"></span>
          </div>          
        </div>
      </div>
    </div>
    <!-- STAFF SECTION END -->
    <?php endif; wp_reset_query(); ?>  
      
    <?php 
      $social = checkFacebookTwitter(get_post_meta( $post->ID, 'wpcf-social-media-account-url', true));
      if(!empty( $social )): 
    ?>
    <!-- SOCIAL MEDIA SECTION BEGIN -->  
    <div class="section">
      <div class="background-image" style="background-image : url('<?php echo get_template_directory_uri() . '/images/header.jpg'; ?>')"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
            <h2 class="section-title section-title-white">Social Media Updates</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
            <ul class="media-list social-media-list">
              <?php 
              if (strpos($social, 'facebook') !== false):
              ?>
                <?php echo do_shortcode('[custom-facebook-feed id=' . $social . ']'); ?>
              <?php else: ?>
              <?php
                $tweets = $twitterCon->get('statuses/user_timeline', array('screen_name' => $social, 'count' => 2));
                if(isset($tweets->errors)):   
                  echo 'Error :'. $tweets->errors[0]->code. ' - '. $tweets->errors[0]->message;
                else:
                for($tweet_count = 0; $tweet_count <= 1; $tweet_count++): 
              ?>
                <li class="media facebook">
                  <div class="col-xs-12">
                    <div class="media-body">
                      <p><?php echo $tweets[$tweet_count]->text; ?></p>                    
                      <p class="pull-right"><?php echo date("F d \a\\t h:i a", strtotime($tweets[$tweet_count]->created_at)); ?></p>
                      <p class="pull-left"><a href="<?php echo get_post_meta( $post->ID, 'wpcf-social-media-account-url', true); ?>">@<?php echo $social; ?></a></p>  
                    </div>
                  </div>
                </li>                
              <?php endfor; endif ?>
            <?php endif; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- SOCIAL MEDIA SECTION END -->  
    <?php endif; ?>
    
    <?php 
      $child_posts = types_child_posts('ministry-blog-post'); 
      if(!empty( $child_posts )):
    ?>  
    <!-- BLOG POST SECTION BEGIN -->  
    <div class="section blog-post-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">
            <h2 class="section-title">Related Posts</h2>
          </div>
        </div>
        <div class="row">
          <?php             
            foreach ($child_posts as $cpost):
              $post_id = wpcf_pr_post_get_belongs($cpost->ID, 'post');       
              $post = get_post($post_id);                      
          ?>
            <div class="col-xs-12 col-sm-4 col-md-offset-0 blog-post-container">
              <div class="col-xs-12 blog-post">
                <a href="<?php the_permalink( $post->ID ); ?>">
                <?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>    
                  <img src="<?php echo $image[0]; ?>" alt="<?php echo $post->post_title; ?>" class="img-responsive blog-featured-image">            
                <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive blog-featured-image" alt="<?php echo $post->post_title; ?>" />
                <?php endif; else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive blog-featured-image" alt="<?php echo $post->post_title; ?>" />
                <?php endif; ?>
                </a>
                <div class="col-xs-12 blog-content">
                  <h3><a href="<?php the_permalink( $post->ID ); ?>" class="blog-post-title"><?php echo $post->post_title; ?></a></h3>
                  <?php echo wp_trim_words( $post->post_content, $num_words = 20, '<a href="' . get_the_permalink( $post->ID ) . '" class="blog-read-more">Read More</a>' ); ?>
                </div>
              </div>
            </div>
            <?php endforeach; ?>

        </div>
      </div>
    </div>
    <!-- BLOG POST SECTION END -->  
    <?php endif; wp_reset_query(); ?>  
      
    <!-- CONTACT SECTION BEGIN -->  
    <div class="section contact-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
            <h2 class="section-title section-title-white">CONTACT <?php echo $mtitle; ?></h2>
            <?php echo do_shortcode( '[gravityform id="4" title="false" description="false"]' ); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- CONTACT SECTION END -->    

<?php endwhile; ?>

<?php
get_footer();