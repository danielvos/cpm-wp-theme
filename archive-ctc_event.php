<?php
/**
 * The template for displaying all events.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-8">
            <h1 class="page-title">UPCOMING EVENTS</h1>
          </div>

            <div class="col-xs-12 col-sm-2 col-md-2 text-right sort-buttons">
              <div class="btn-group btn-group-sm">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Browse By <span class="fa fa-caret-down"></span></button>
                <?php
                  $event_category = get_terms( 'ctc_event_category' );
                  if ( ! empty( $event_category ) && ! is_wp_error( $event_category ) ){
                    echo '<ul class="dropdown-menu" role="menu">';
                    foreach ( $event_category as $ecat ) {                      
                      echo '<li><a href="?category=' . $ecat->slug . '">' . $ecat->name . '</a></li>';
                    }
                   echo '</ul>';
                  }
                ?>
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-2 sort-buttons">
              <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                  <input type="hidden" name="search_type" value="events" />
                  <div class="input-group input-group-sm">
                    <input type="text" name="s" id="s" class="form-control" placeholder="Search Events" />
                  </div>
              </form>
            </div>          

        </div>
      </div>
    </div>
      
    <!-- EVENT SECTION BEGIN -->  
    <div class="section event-section ">
      <div class="container">
        <?php if ( have_posts() ) : ?>
          <div class="row">
            <div class="col-xs-12 event-container">
            <?php  while ( have_posts() ) : the_post(); ?>
              <div class="row event-row">
                <div class="col-xs-12 col-sm-8">
                  <a href="<?php the_permalink(); ?>"><h3 class="event-title"><?php the_title(); ?></h3></a>
                  <ol class="list-inline event-details-list">
                    <?php if(get_post_meta( $post->ID, '_ctc_event_start_date', true) != get_post_meta( $post->ID, '_ctc_event_end_date', true)): ?>
                    <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("M d", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?> - <?php echo date("M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_end_date', true))); ?></li>
                    <?php else: ?>
                    <li><i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php echo date("D, M d, Y", strtotime(get_post_meta( $post->ID, '_ctc_event_start_date', true))); ?></li>
                    <?php endif; ?>
                    <?php if(get_post_meta( $post->ID, '_ctc_event_hide_time_range', true) == 1): ?>
                    <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo get_post_meta( $post->ID, '_ctc_event_time', true); ?></li>
                    <?php else: ?>
                    <li><i class="event-detail-icon fa fa-clock-o fa-fw fa-lg"></i><?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_start_time', true))); ?> - <?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_ctc_event_end_time', true))); ?></li>
                    <?php endif; ?>
                    <?php if(!empty(get_post_meta( $post->ID, '_ctc_event_venue', true))): ?>
                    <li><i class="event-detail-icon fa fa-fw fa-lg fa-location-arrow"></i><?php echo get_post_meta( $post->ID, '_ctc_event_venue', true); ?></li>
                    <?php endif; ?>
                    <li><i class="fa fa-2x fa-fw fa-refresh event-detail-icon"></i><?php echo ucfirst(get_post_meta( $post->ID, '_ctc_event_recurrence', true)); ?></li>
                  </ol>
                  <?php the_excerpt(); ?>
                </div>
                <a href="<?php the_permalink(); ?>">
                  <div class="hidden-xs col-sm-12 col-sm-4 event-details-image-container">
                    <?php if (has_post_thumbnail( $post->ID ) ):
                      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                      if($image[0]):
                    ?>      
                      <img src="<?php echo $image[0]; ?>" class="event-details-image img-responsive" alt="<?php the_title(); ?>">        
                    <?php else: ?>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="event-details-image img-responsive" alt="<?php the_title(); ?>"> 
                    <?php endif; else: ?>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="event-details-image img-responsive" alt="<?php the_title(); ?>"> 
                    <?php endif; ?>   
                  </div>
                </a>
              </div>
              <?php endwhile; ?>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <?php numeric_posts_navigation(); ?>
            </div>
          </div>

          <?php else: ?>
            <div class="row">
              <div class="col-md-12">
                <h3>Nothing Found!</h3>
              </div>
            </div>
          <?php endif; ?>      
      </div>
    </div>
    <!-- EVENT SECTION END --> 

<?php
get_footer();