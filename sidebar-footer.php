<?php
/**
 * The sidebar containing the footer widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package crosspoint
 */
?>


<?php if(is_active_sidebar( 'footer-1' )): ?>
<div class="col-xs-12 col-sm-6">
	<?php dynamic_sidebar( 'footer-1' ); ?>         
</div>
<?php endif; ?>

<?php if(is_active_sidebar( 'footer-2' )): ?>
<div class="col-xs-12 col-sm-2">
	<?php dynamic_sidebar( 'footer-2' ); ?>         
</div>
<?php endif; ?>

<?php if(is_active_sidebar( 'footer-3' )): ?>
<div class="col-xs-12 col-sm-2">
	<?php dynamic_sidebar( 'footer-3' ); ?>         
</div>
<?php endif; ?>

<?php if(is_active_sidebar( 'footer-4' )): ?>
<div class="col-xs-12 col-sm-2">
	<?php dynamic_sidebar( 'footer-4' ); ?>         
</div>
<?php endif; ?>

