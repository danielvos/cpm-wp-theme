<?php
/**
 * Framework Styles
 */

/**
 * Registers framework styles.
 */
function tbcf_register_styles() {

	wp_enqueue_style( 'tbcf', tbcf_get_template_directory_uri() . '/inc/church-theme/assets/css/tbcf.css' );

}
add_action( 'wp_enqueue_scripts', 'tbcf_register_styles' );
