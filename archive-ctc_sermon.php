<?php
/**
 * The template for displaying all messages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

<?php           
  $series = get_the_terms( $post->ID, 'ctc_sermon_series' ); 
  $speaker = get_the_terms( $post->ID, 'ctc_sermon_speaker' ); 
?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <h1 class="page-title">Messages</h1>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 text-right sort-buttons">
            <div class="btn-group btn-group-sm">
              <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Browse By Series   <span class="fa fa-caret-down"></span></a>
              <?php
                $series_category = get_terms( 'ctc_sermon_series' );
                if ( ! empty( $series_category ) && ! is_wp_error( $series_category ) ){
                  echo '<ul class="dropdown-menu" role="menu">';
                  foreach ( $series_category as $secat ) {                      
                    echo '<li><a href="' . get_term_link( $secat->slug, 'ctc_sermon_series' ) . '">' . $secat->name . '</a></li>';
                  }
                 echo '</ul>';
                }
            ?>
            </div>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 text-right sort-buttons">
            <div class="btn-group btn-group-sm">
              <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Browse By Speaker   <span class="fa fa-caret-down"></span></a>
              <?php
                $speaker_category = get_terms( 'ctc_sermon_speaker' );
                if ( ! empty( $speaker_category ) && ! is_wp_error( $speaker_category ) ){
                  echo '<ul class="dropdown-menu" role="menu">';
                  foreach ( $speaker_category as $scat ) {                      
                    echo '<li><a href="' . get_term_link( $scat->slug, 'ctc_sermon_speaker' ) . '">' . $scat->name . '</a></li>';
                  }
                 echo '</ul>';
                }
            ?>
            </div>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 sort-buttons">
            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                <input type="hidden" name="search_type" value="messages" />
                <div class="input-group input-group-sm">
                  <input type="text" name="s" id="s" class="form-control" placeholder="Search Messages" />
                </div>
            </form>
          </div>

        </div>
      </div>
    </div>
      
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="active">Messages</li>
            </ul>
          </div>
        </div>         
        <div class="row">
          <!-- MAIN CONTENT AREA -->
          <div class="col-xs-12 col-sm-8">
          <?php 
            $posts = new WP_Query( 'post_type=ctc_sermon&posts_per_page=1' );
            if ($posts->have_posts()):
            while ($posts->have_posts()): $posts->the_post();
          ?>
              <div class="embed-responsive embed-responsive-16by9">
                <?php 
                  if (has_post_thumbnail( $post->ID ) ): 
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                    if($image[0]):
                ?>
                    <img src="<?php echo $image[0]; ?>" class="img-responsive" id="playerImage" alt="<?php the_title(); ?>">
                  <?php else: ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" id="playerImage" class="img-responsive" alt="<?php the_title(); ?>">
                  <?php endif; ?>                
                <?php elseif(!empty(types_render_termmeta( "message-series-image", array("term_id" => $post->ID)))):
                  echo types_render_termmeta( "message-series-image", array("term_id" => $post->ID, "alt" => get_the_title(), "class" => "img-responsive") );
                  else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" id="playerImage" class="img-responsive" alt="<?php the_title(); ?>">
                <?php endif; ?>

                <?php if(!empty(get_post_meta( $post->ID, '_ctc_sermon_video', true))): ?>
                  <?php echo generate_video(get_post_meta( $post->ID, '_ctc_sermon_video', true)); ?>
                <?php endif; ?>

                <?php if(!empty(get_post_meta( $post->ID, '_ctc_sermon_audio', true))): ?>
                  <div id="playerAudio">
                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                    <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
                      <div class="jp-type-single">
                        <div class="jp-gui jp-interface">
                          <div class="jp-controls">
                            <button class="jp-play" role="button" tabindex="0">play</button>
                            <button class="jp-stop" role="button" tabindex="0">stop</button>
                          </div>
                          <div class="jp-progress">
                            <div class="jp-seek-bar">
                              <div class="jp-play-bar"></div>
                            </div>
                          </div>
                          <div class="jp-volume-controls">
                            <button class="jp-mute" role="button" tabindex="0">mute</button>
                            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                            <div class="jp-volume-bar">
                              <div class="jp-volume-bar-value"></div>
                            </div>
                          </div>
                          <div class="jp-time-holder">
                            <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                            <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                            <div class="jp-toggles">
                              <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                            </div>
                          </div>
                        </div>
                        <div class="jp-details">
                          <div class="jp-title" aria-label="title">&nbsp;</div>
                        </div>
                        <div class="jp-no-solution">
                          <span>Update Required</span>
                          To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                        </div>
                      </div>
                    
                    </div>
                  </div>

                  <script>
                    jQuery(document).ready(function($) {
                      $("#jquery_jplayer_1").jPlayer({
                            ready: function () {
                              $(this).jPlayer("setMedia", {
                                title: "<?php the_title(); ?>",
                                mp3: "<?php echo get_post_meta( $post->ID, '_ctc_sermon_audio', true); ?>"
                              })
                            },
                            cssSelectorAncestor: "#jp_container_1",
                            swfPath: "/js",
                            supplied: "mp3",
                            useStateClassSkin: true,
                            autoBlur: false,
                            smoothPlayBar: true,
                            keyEnabled: true,
                            remainingDuration: true,
                            toggleDuration: true
                        });

                        $("#audio-play-button").on('click', function(){
                          $("#jquery_jplayer_1").jPlayer("play");
                        });

                        $("#video-play-button").on('click', function(){
                          $("#jquery_jplayer_1").jPlayer("pause", 10);
                        });
                    });
                  </script>
                <?php endif; ?>

                <div id="video-overlay" class="btn-group video-overlay">                   
                  <?php if(!empty(get_post_meta( $post->ID, '_ctc_sermon_video', true))): ?>
                    <a href="#" class="btn btn-link" id="video-play-button"><i class="fa fa-2x fa-fw fa-play"></i></a>
                  <?php endif; ?>
                  <?php if(!empty(get_post_meta( $post->ID, '_ctc_sermon_audio', true))): ?>
                    <a href="#" class="btn btn-link" id="audio-play-button"><i class="fa fa-2x fa-fw -audio-o fa-volume-up"></i></a>
                  <?php endif; ?>
                  <?php if(!empty(get_post_meta( $post->ID, '_ctc_sermon_audio', true))): ?>
                    <a href="<?php echo get_post_meta( $post->ID, '_ctc_sermon_audio', true); ?>" class="btn btn-link"><i class="fa fa-2x fa-fw fa-download"></i></a>
                  <?php endif; ?>
                  <?php if(!empty(get_post_meta( $post->ID, '_ctc_sermon_pdf', true))): ?>
                     <a href="<?php echo get_post_meta( $post->ID, '_ctc_sermon_pdf', true); ?>" target="_blank" class="btn btn-link"><i class="fa fa-2x fa-fw fa-file-pdf-o"></i></a>
                  <?php endif; ?>               
                </div>
              </div>

              <div class="row">
                <div class="col-xs-12 col-sm-8">
                  <h3><?php the_title(); ?></h3>
                </div>
                <div class="col-xs-12 col-sm-4 text-right message-share">
                  <!-- <a class="btn btn-lg btn-primary">Share This</a> -->
                  <?php do_action( 'addthis_widget', get_permalink(), get_the_title(), array(
                      'type' => 'custom',
                      'size' => '32', // size of the icons.  Either 16 or 32
                      'services' => 'facebook,twitter,linkedin', // the services you want to always appear
                      'preferred' => '0', // the number of auto personalized services
                      'more' => true, // if you want to have a more button at the end
                      'counter' => 'bubble_style' // if you want a counter and the style of it
                      ));
                  ?>
                </div>
              </div>

              <div class="row message-meta-row">
                <div class="col-xs-12">
                  <ol class="list-inline metadata-list">
                    <li>
                      <i class="event-detail-icon fa fa-calendar fa-fw fa-lg"></i><?php the_date(); ?></li>
                    <li>
                      <i class="event-detail-icon fa fa-fw fa-lg fa-microphone"></i><?php echo $speaker[0]->name; ?></li>
                    <?php if(!empty(get_post_meta( $post->ID, 'wpcf-scripture-reference', true))): ?>
                    <li>
                      <i class="event-detail-icon fa fa-fw fa-lg fa-book"></i><?php echo get_post_meta( $post->ID, 'wpcf-scripture-reference', true); ?></li>
                    <?php endif; ?>
                  </ol>
                </div>
              </div>
            
            <?php 
              endwhile;
              endif;
            ?>

              <div class="row other-messages">
                <div class="col-xs-12">
                  <strong class="big-text black">Other Messages in This Series</strong>
                </div>
                <div class="col-xs-12">
                  <?php 
                    $custom_taxterms = wp_get_object_terms( $post->ID, 'ctc_sermon_series', array('fields' => 'ids') );
                    $args = array(
                      'post_type' => 'ctc_sermon',
                      'post_status' => 'publish',
                      'posts_per_page' => -1,
                      'orderby'   => 'id',
                      'order' => 'DESC',
                      'tax_query' => array(
                          array(
                              'taxonomy' => 'ctc_sermon_series',
                              'field' => 'id',
                              'terms' => $custom_taxterms
                          )
                      ),
                      'post__not_in' => array ($post->ID),
                      );
                    $related_items = new WP_Query( $args );

                    if ($related_items->have_posts()) :
                      echo '<ul class="list-group message-list">';
                      while ( $related_items->have_posts() ) : $related_items->the_post();
                        $rp_speaker = get_the_terms( $post->ID, 'ctc_sermon_speaker' ); 
                  ?>
                        <li class="list-group-item">
                          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                          <span><i class="fa fa-calendar fa-fw"></i> <?php echo get_the_date('F j, Y'); ?></span>
                          <span><i class="fa fa-fw fa-microphone"></i><?php echo $rp_speaker[0]->name; ?></span>
                        </li>
                  <?php
                        endwhile;
                      echo '</ul>';
                    endif;

                    wp_reset_postdata();
                  ?>
                </div>
              </div>              
              
          </div>
          
          <!-- SIDEBAR -->
          <div class="col-xs-12 col-sm-4 ">
            <?php if(!empty(types_render_termmeta( "message-series-image", array("term_id" => $series[0]->term_id)))): 
              echo types_render_termmeta( "message-series-image", array("term_id" => $series[0]->term_id, "alt" => $series[0]->name, "class" => "img-responsive") );
            ?>
            <?php else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive" alt="<?php echo $series[0]->name; ?>"> 
            <?php endif; ?>
            
            <strong class="big-text black">Current Series:</strong>
            <h3 class="margin-bottom-10"><?php echo $series[0]->name; ?></h3>
            <p><?php echo $series[0]->description; ?></p>           
          </div>
            
        </div>
      </div>
    </div>

    <div class="section message-series-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="section-title">BROWSE BY SERIES</h2>
          </div>
        </div>

        <div class="row">
          <?php 
            $results = get_series_categories();
            $catsArray = [];
            foreach($results as $key => $res){
              $catsArray[] = $key;
            }

            $mseries = get_terms( array(
              'taxonomy' => 'ctc_sermon_series',
              'number'  =>  6,
              'include' => $catsArray,
              'orderby'  => 'include',
            ) );

            $count = 1;
            foreach($mseries as $ser):
          ?>
            <div class="col-sm-4 series-box"<?php echo (($count == 4) ? ' style="clear: left;"' : ''); ?>>
              <a href="<?php echo get_term_link( $ser->slug, 'ctc_sermon_series' ) ?>">
              <div class="series-box-container">
                <?php if(!empty(types_render_termmeta( "message-series-image", array("term_id" => $ser->term_id)))): 
                  echo types_render_termmeta( "message-series-image", array("term_id" => $ser->term_id, "alt" => $ser->name, "class" => "img-responsive") );
                ?>
                <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive" alt="<?php echo $ser->name; ?>"> 
                <?php endif; ?>
                <div class="box-content">
                  <h5><?php echo $ser->name; ?></h5>
                </div>
              </div>
              </a>
            </div>
          <?php $count++; endforeach; ?>
        </div>

        <div class="row">
            <div class="col-xs-12 text-center">
              <a href="<?php echo get_permalink( get_page_by_path( 'sermon-series' ) ); ?>" class="btn btn-primary">View All Series</a>
            </div>
        </div>

      </div>
    </div>

<?php
get_footer();