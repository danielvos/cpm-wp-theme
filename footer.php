<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package crosspoint
 */

?>

	</div><!-- #content -->

	<!-- FOOTER SECTION BEGIN -->  
    <!-- Email Subscription -->  
    <div class="section" id="email-subscription-section">
      <div class="background-image" style="background-image : url('<?php echo get_template_directory_uri(); ?>/images/HomePageNewsletterPhoto.jpg')"></div>    
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3 class="section-title section-title-white">Join Our Mailing List</h3>
            <p>Our weekly email newsletter is how we let you know what’s happening and what’s next at CrossPoint.</p>
          </div>
          <div class="col-md-6">
            <?php echo do_shortcode( '[gravityform id="3" title="false" description="false"]' ); ?>
          </div>            
        </div>
      </div>      
    </div>   

    <div class="section footer-section">
      <div class="container">
        <div class="row">            
            <?php get_sidebar( 'footer' ); ?>
        </div>
      </div>
    </div>
    <!-- FOOTER SECTION END -->

<?php wp_footer(); ?>

</body>
</html>
