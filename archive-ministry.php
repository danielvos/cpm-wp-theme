<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-8">
            <h1 class="page-title">Ministries</h1>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 text-right sort-buttons">
              <div class="btn-group btn-group-sm">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Browse By <span class="fa fa-caret-down"></span></button>
                <?php
                  $event_category = get_terms( 'ministry-category' );
                  if ( ! empty( $event_category ) && ! is_wp_error( $event_category ) ){
                    echo '<ul class="dropdown-menu" role="menu">';
                    foreach ( $event_category as $ecat ) {                      
                      echo '<li><a href="../ministry-category/' . $ecat->slug . '">' . $ecat->name . '</a></li>';
                    }
                   echo '</ul>';
                  }
                ?>
              </div>
            </div>

          <div class="col-xs-12 col-md-2 sort-buttons">
          	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
              <input type="hidden" name="search_type" value="ministry" />
	            <div class="input-group input-group-sm">
	              <input type="text" name="s" id="s" class="form-control" placeholder="Search Ministries" />
	            </div>
	        </form>
          </div>
        </div>
      </div>
    </div>

    <div class="section section-archive">
      <div class="container">
        <div class="row">

          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="active">Ministries</li>
            </ul>
          </div>
        </div>

        <?php 
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

          $args = array(
            'post_type' => 'ministry',
            'post_status' => 'publish',
            'posts_per_page' => 9,
            'paged'  =>  $paged,
            'orderby'   => 'title',
            'order' => 'ASC',
            );
          $items = new WP_Query( $args );
        ?>

        <div class="row">
        <?php if ( $items->have_posts() ) :
        	while ( $items->have_posts() ) : $items->the_post(); ?>
            <div class="col-xs-12 col-sm-4 col-md-offset-0 blog-post-container">
              <div class="col-xs-12 blog-post" style="min-height: inherit;">
              	<?php if (has_post_thumbnail( $post->ID ) ):
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                  if($image[0]):
                ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" /></a>                  
                <?php else: ?>
                  <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; else: ?>
                <a href="<?php the_permalink() ?>"><img class="img-responsive blog-featured-image" src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" alt="<?php the_title(); ?>" /></a>
                <?php endif; ?>
                <div class="col-xs-12 blog-content">
                  <h3><a href="<?php the_permalink() ?>" class="blog-post-title"><?php the_title(); ?></a></h3>
                </div>
              </div>
            </div>
		<?php endwhile; ?>
        </div>       

        <div class="row">
          <div class="col-md-12">
          	<?php numeric_posts_navigation(); ?>
          </div>
        </div>
    	<?php else: ?>
    		<div class="row">
	          <div class="col-md-12">
	          	<h3>Nothing Found!</h3>
	          </div>
	        </div>
        <?php endif; ?>
      </div>
    </div>

<?php
get_footer();
