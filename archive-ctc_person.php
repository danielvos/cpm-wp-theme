<?php
/**
 * The template for displaying all persons.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crosspoint
 */

get_header(); ?>

<?php 
  $person_category = get_terms( 'ctc_person_group' );
?>

	<div class="page-title-section section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-8">
            <h1 class="page-title">STAFF</h1>
          </div>

          <div class="col-xs-12 col-sm-3 col-md-2 text-right sort-buttons">
            <div class="btn-group btn-group-sm">
              <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Choose a Team <span class="fa fa-caret-down"></span></a>
              <?php
                  if ( ! empty( $person_category ) && ! is_wp_error( $person_category ) ){
                    echo '<ul class="dropdown-menu" role="menu">';
                    foreach ( $person_category as $pcat ) {                      
                      echo '<li><a href="' . get_term_link( $pcat->slug, 'ctc_person_group' ) . '">' . $pcat->name . '</a></li>';
                    }
                   echo '</ul>';
                  }
              ?>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-2 sort-buttons">
            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
              <input type="hidden" name="search_type" value="person" />
              <div class="input-group input-group-sm">
                <input type="text" name="s" id="s" class="form-control" placeholder="Search People" />
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="section" style="padding-bottom: 0;">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="breadcrumb breadcrumb-container">
              <li class="breadcrumb">
                <a href="//<?php echo getenv('HTTP_HOST'); ?>">CrossPoint</a>
              </li>
              <li class="active">People</li>
            </ul>
          </div>
        </div> 
      </div>
    </div>

      <?php 
          if( $person_category ) :
            $sliderCount = 1;
          foreach( $person_category as $term ) :
      ?>
      <?php if($sliderCount % 2 == 0): ?>
        <div class="section section-slider staff-section-alternate-bg" id="carousel-<?php echo $sliderCount; ?>">
      <?php else: ?>
        <div class="section section-slider" id="carousel-<?php echo $sliderCount; ?>">
      <?php endif; ?>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="section-title"><?php echo $term->name; ?></h2>
          </div>
      <?php
              $args = array(
                'post_type'             => 'ctc_person',
                'posts_per_page'        => -1,
                'post_status'           => 'publish',
                'orderby'               => 'title', 
                'order'                 => 'ASC',
                'tax_query'             => array(
                                            array(
                                                'taxonomy' => 'ctc_person_group',
                                                'field'    => 'slug',
                                                'terms'    => $term->slug,
                                            ),
                                        ),
                'ignore_sticky_posts'   => true
              );
              $_posts = new WP_Query( $args );

              if( $_posts->have_posts() ) :
      ?>
            <div class="col-xs-12 carouFredSel-wrap white-bg">
              <div class="carouFredSel preSlider">
      <?php
                  while( $_posts->have_posts() ) : $_posts->the_post();
      ?>
                      <div class="item">
                        <a href="<?php the_permalink(); ?>">
                        <?php if (has_post_thumbnail( $post->ID ) ):
                          $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-image' );
                          if($image[0]):
                        ?>    
                          <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" class="center-block img-responsive ministry-image">            
                        <?php else: ?>
                          <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive staff-image" alt="<?php the_title(); ?>" />
                        <?php endif; else: ?>
                          <img src="<?php echo get_template_directory_uri(); ?>/images/crosspoint-fallback.png" class="img-responsive staff-image" alt="<?php the_title(); ?>" />
                        <?php endif; ?>
                      </a>
                        <div class="slide-details">
                          <a href="<?php the_permalink(); ?>"><h3 class="staff-name"><?php the_title(); ?></h3></a>
                          <p><?php echo get_post_meta( $post->ID, '_ctc_person_position', true); ?></p>
                        </div>
                      </div>
      <?php
                  endwhile;
      ?>
              </div>
              <span class="fa fa-chevron-right black slide-next"></span>
            </div>
      <?php
              endif;
              wp_reset_postdata();
      ?>          
        </div>
        </div>
      </div>
      <?php
          $sliderCount++;
          endforeach;
        endif;
      ?>

<?php
get_footer();